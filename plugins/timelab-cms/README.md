timelab-cms
===========

Timelab CMS admin plugin för Wordpress

Du kommer åt data från CMSet genom `$cms` variablen.

Konfiguration
------
I temats mapp så skapar du en fil med namnet `theme-settings.json`. Denna kan användas för att konfigurera vissa saker i adminet.

Inställningar som går att sätta:

`menu-max-levels` - Bestämmer hur många nivåer en meny kan ha i menyhanteraren i adminet.
`personnel-image-width` & `personnel-image-height` - Bestämmer hur stora personal bilderna ska vara.

Exempel på en konfig:
````json
{
  "menu-max-levels": 2,
  "personnel-image-width": 256,
  "personnel-image-height": 256
}
````

CSS
---
Det går även att overrida CSSen i adminet genom att skapa en fil som heter `timelab_cms_custom.css`, om CMSet upptäcker
denna fil så kommer den att inkludera den i adminet.

Används bland annat för att styla om tredjaparts plugins så dom ser mer konsekvent ut med vårat CMS!

Javascript
----------
Ifall det är något man vill lägga in genom javascript i adminet för den specificka sidan så går det att göra genom att
skapa `timelab_cms_custom.js` i rooten av det aktuella temat.

Moduler
-------

### Admin
Detta är huvudadministrations modulen som rensar bort onödiga menyalternativ samt lägger in admin stylesheetet

### Dashboard
Denna modul är den som ersätter Wordpress default dashboardet med den nya dashboarden.

Du kan lägga in en ny snabbknapp genom att kalla på `$dashboard->add_dashboard_quick_button()`, följande skriver ut snabbknappen för bildspel:

````php
$dashboard->add_dashboard_quick_button('Bildspel', 'Lägg till och ta bort bilder från dina bildspel', admin_url('admin.php?page=timelab_cms_sliders'), 'glyphicon-camera');
````

### Undersidor
Denna modul är den som hanterar listningen samt editorn för undersidor.

### Bildspel
Denna modul hanterar listningen samt editorn för bildspel

### Menyhanterare
Denna modul lägger till en ny editor för att hantera menyer

### Personal
Denna modul lägger till en simpel editor för att lägga till och ändra personal

*Denna modul är ej påbörjad*

### Märkeshanterare (Optional)
Denna modul gör det möjligt för kunden att modifera märken (tex Bilmärken för en bilhandlare) som ska visas på sidan.

Implementeringsinstruktioner
-----------------------------

### Logotyp

För att skriva ut kundens logga så kör du `get_customer_logo()`

Nedan kommer ett exempel som skriver ut loggan:

````php
<img class="img-responsive" src="<?php echo $cms->get_customer_logo(); ?>" />
````

### Anläggning

För att hämta ut en anläggning så kör du `$cms->facilities->get_facility($query)`.

`get_facility($query)` tar emot både ett anläggnings id och en anläggnings slug.

Exempel för att skriva ut en anläggnings namn:

````php
$facility = $cms->facilities->get_facility('lulea');

echo $facility->name;
````

Det går även att hämta ut samtliga anläggnignar, för detta så kör du `$cms->facilities->get_facilities()`.
Detta returnerar ut en array med samtliga anläggningar sorterade i samma ordning som i adminet.

Exempel för att skriva ut samtliga anläggningars namn:

````php
$facilities = $cms->facilities->get_facilities;

foreach ($facilities as $facility) {
    echo $facility->name;
}
````

### Address

För att hämta ut en anläggnings address så måste du först ha hämtat ut anläggningen dit adressen är kopplad.
I följande instruktioner antar vi att du hämtat ut anläggningen och satt den i `$facility`.

För att hämta ut en adress så använder du dig utav `$facility->get_address()`

En address består av:
* `formatted` - Detta är den formatterade addressen. Detta är det man oftast vill skriva ut på sidan. Formatet är: "Kungsgatan 34A, 972 41 Luleå, Sverige"
* `city` - Staden
* `street` - Gatuadressen
* `zip` - Postkod
* `lat` och `long` - Latitud koordinaten för adressen. Finns primärt för Google Maps eller annan karta.

Nedan har vi ett exempel där vi skriver ut den formatterade adressen.

````php
$address = $facility->get_address();

echo $address->formatted;
````

### Öppettider

För att hämta ut en anläggnings öppettider så måste du först ha hämtat ut anläggningen dit adressen är kopplad.
I följande instruktioner antar vi att du hämtat ut anläggningen och satt den i `$facility`.

För att hämta ut en array med samtliga öppettider så använder du dig utav `$facility->get_opening_hours()`

Öppettider består av:
* `title` - Titlen för tiden. Tex "Måndag"
* `from` - Från tiden. Tex "08.00" eller "Stängt"
* `to` - Till tiden. Tex "18.00"

Nedan har vi ett exempel där vi skriver ut samtliga öppettider.

````php
$oppettider = $facility->get_opening_hours();

echo "<ul>";
foreach ($oppettider as $tid) {
    // Ifall "to" är blankt, skriv då bara ut "from".
    $mall = (!empty($tid->to)) ? "<li><b>%s:</b> %s - %s</li>" : "<li><b>%s:</b> %s</li>";
    echo sprintf($mall, $tid->title, $tid->from, $tid->to);
}
echo "</ul>";
````

### Kontaktuppgifter

För att hämta ut en anläggnings kontaktupggifter så måste du först hämta ut anläggningen dit kontaktupggifterna är kopplad.
I följande instruktioner antar vi att du hämtat ut anläggningen och satt den i `$facility`.

sedan kör du `$facility->get_contact_details()` för att få ut en array med samtliga kontakt uppgifter.

En kontaktuppgift består av:
* `type` - Typen av kontaktuppgift. Tex 'email'
* `value` - Det råa värdet på kontaktuppgiften. Tex 'patrik@timelab.se'

Om detta ska skrivas ut på sidan så rekommenderas dock att man hämtar ut dom formatterade värdena. för att göra detta så kör man tex `$kontaktuppgift->get_name()` och `$kontaktuppgift->get_formatted_value()`

Nedan har vi ett exempel där vi skriver ut samtliga kontaktuppgifter för en anläggning i en lista:

````php
        $kontaktuppgifter = $facility->get_contact_details();
        echo "<ul>";
        foreach ($kontaktuppgifter as $kontaktuppgift) {

            if (empty($kontaktuppgift->value)) continue;

            echo "<li>" . $kontaktuppgift->get_name() . ": " . $kontaktuppgift->get_formatted_value() . "</li>";
        }
        echo "</ul>";
````


### Personal

För att hämta ut personal så måste du först hämta ut anläggningen dit personalen är kopplad.
I följande instruktioner antar vi att du hämtat ut anläggningen och satt den i `$facility`.

sedan kör du `$facility->get_personnel()` för att få ut en array med samtlig personal, sorterad i samma ordning som i
administrationen.

En person består av:
* `name` - Namnet på personen
* `role` - Personens roll

för att få ut en bild på personen så måste du köra `$person->get_image()`.

För att få ut kontaktuppgifter så måste man använda sig utav `$person->get_contact_details()` som returnerar en array med alla kontaktuppgifter.

En kontaktuppgift består av:
* `type` - Typen av kontaktuppgift. Tex 'email'
* `value` - Det råa värdet på kontaktuppgiften. Tex 'patrik@timelab.se'

Om detta ska skrivas ut på sidan så rekommnderas dock att man hämtar ut dom formatterade värdena. för att göra detta så kör man `$kontaktuppgift->get_name()` och `$kontaktuppgift->get_formatted_value()`

Nedan har vi ett exempel där vi skriver ut samtlig personal med all data i en lista:

````php
$personal = $facility->get_personnel();

echo "<ul>";
foreach ($personal as $person) {
    // Person
    echo "<li>";
        echo "<ul>";
        echo "<li><img src='" . $person->get_image() . "' /></li>"; // Hämtar ut bilden
        echo "
           <li>Namn: {$person->name}</li>
           <li>Roll: {$person->role}</li>
           ";

        $kontaktuppgifter = $person->get_contact_details();

        // Kontaktuppgifter
        echo "<ul>";
        foreach ($kontaktuppgifter as $kontaktuppgift) {

            if (empty($kontaktuppgift->value)) continue;

            echo "<li>" . $kontaktuppgift->get_name() . ": " . $kontaktuppgift->get_formatted_value() . "</li>";
        }
        echo "</ul>"; // End Kontaktuppgifter

        echo "</ul>"; // End Person
    echo "</li>";
}
echo "</ul>"; // End Personal
````

### Bildspel

för att skriva ut bildspelet så kör du `get_slider_images($id)` där id är sliderns id. Så i en page template skriver du `$cms->slider->get_slider_images($id)` för att få en array med samtliga bilder.

Bildobjektet innehåller följande:

* `url` - Länken till bilden
* `href` - Länken dit bilden ska länkas
* `target` - Target på länken (antingen _blank eller _self)
* `title` - Rubriken för bilden, returnerar `null` ifall rubriker är inaktiverade för bildspelet
* `subtitle` - Underrubriken för bilden, returnerar `null` ifall underrubriker är inaktiverade för bildspelet
* `text` - Bildtexten för bilden

Nedan kommer ett exempel som loopar ut alla bilderna i ett bildspel:

````php
$slides = $cms->slider->get_slider_images(433);

foreach ($slides as $slide) {
    echo "<li>";
    echo "<h2>{$slide->title}</h2>";
    echo "<h3>{$slide->subtitle}</h3>";
    echo "<a href='{$slide->href}' target='{$slide->target}'>";
    echo "<img src='{$slide->url}' />";
    echo "<p>{$slide->text}</p>";
    echo "</a>";
    echo "</li>";
}
````

## Bildgalleri

**Informationen nedan är WIP och kan ändras!!!**

För att hämta ut samtliga bilder från galleriet så kör du `get_images()`, Så i en page template skriver du `$cms->gallery->get_images()`

Bild objektet innehåller bland annat:
`tags` - En array med samtliga taggar accosierad med bilden

För att hämta ut samtliga taggar på sidan så kör du `$cms->gallery->get_tags()` för att få ut en array med samtliga taggar.
För att skriva ut taggens rubrik och värde så gör du följande:


````php
    foreach ($tags as $name=>$value) {
        echo '<li><input type="checkbox" name="tags[]" value="' . $value . '" />' . $name . '</li>';
    }
````


