function bindDirtyChecker() {
    // Spara orginal värdet på samtliga fält
    $('input, textarea, select').each(function () {
        if (!$(this).data('original-value')) {
            $(this).data('original-value', $(this).val());
        }
    });

    // Bind onchange eventet för alla fält
    $('input, textarea, select').unbind('change');
    $('input, textarea, select').change(function () {

        // Ignorera alla fält som har klassen 'transient' då dessa är fält som inte sparas
        if ($(this).hasClass('transient')) {
            return;
        }

        // Om fältet är hidden, lägg dirty klassen på föräldern
        var parent = ($(this).attr('type') !== 'hidden') ? false : true,
            $target = (!parent) ? $(this) : $(this).parent();

        // Kolla om fältet har ett ändrat värde, flagga som dirty om så är fallet
        if ($(this).val() !== $(this).data('original-value')) {
            $target.addClass('dirty');
        } else {
            $(this).removeClass('dirty');
        }

        // Om klassen är hidden, kolla då om samtliga av förälderns barn är clean innan man avflaggar den
        if (parent) {
            var all_clean = true;
            $target.children('input:hidden, textarea:hidden, select:hidden').each(function () {
                if ($(this).val() !== $(this).data('original-value') && !$(this).hasClass('transient')) {
                    all_clean = false;
                }
            });

            if (all_clean) {
                $target.removeClass('dirty');
            }
        }
    });
}

$(function () {
    bindDirtyChecker();

    // När ett formulär submitas så måste det flaggas att det är sparat så den inte dirty checkar
    $('form').submit(function(e) {
       window.savedChanges = true;
    });

    $(window).on('beforeunload', function() {
        // Om ändringar inte är sparade och det finns minst ett dirty fält, berkäfta att användaren vill lämna sidan
        if ($('.dirty').length > 0 && window.savedChanges !== true) {
            return "Alla osparade ändringar kommer att gå förlorad.";
        }
    });
});