function get_sub_menu(list, parent_id, top_menu_id) {
    var menu = {};

    var content = list.children('div');

    menu.title = list.children('div').text();

    menu.id = content.children('input[name*="menu_item_id"]').val();

    content.children('input[name*="menu_item_parent"]').val(parent_id);
    content.children('input[name*="menu_item_menu_id"]').val(top_menu_id);

    menu.children = [];
    
    list.children('ol').children('li').each(function () {
        var sub_menu = get_sub_menu($(this), menu.id, top_menu_id);
        menu.children.push(sub_menu);
    });

    return menu;
}

function get_data_from_list() {

    $('#menu-editor-list li > div').each(function (index) {
        $(this).children('input[name*="menu_item_order"]').val(index+1);
    });

    var hierarchy = [];

    $('#menu-editor-list .top-menu').each(function () {
        var top_menu = {};

        top_menu.title = "Topp meny";
        top_menu.children = [];

        top_menu.id = $(this).data('menu_id');

        $(this).children('li').each(function () {
            var sub_menu = get_sub_menu($(this), 0, top_menu.id);

            top_menu.children.push(sub_menu);
        });

        hierarchy.push(top_menu);
    });
}

$(function () {
    $('#menu-editor-list .top-menu').nestedSortable({
        maxLevels: (window.menu_max_levels) ? window.menu_max_levels : 3,
        tabSize: 10,
        opacity: 0.8,
        forcePlaceholderSize: true,
        placeholder: 'placeholder',
        update: function(event, ui) {
            get_data_from_list();
            $('.top-menu input').each(function () {
                $(this).trigger('change');
            });
        },
        tolerance: 'pointer',
        toleranceElement: '> div',
        items: 'li',
        connectWith: '.top-menu',
        isTree: true,
        doNotClear: true,
        startCollapsed: false
    });

    $('#new-page-modal .template').click(function() {
        $('#new-page-modal .template').removeClass('btn-primary');
        $(this).addClass('btn-primary');
        $('input[name="page_template"]').val($(this).data('template')).trigger('blur').trigger('change');
    });

    $('.collapse-button').on('click', function() {
        $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');

        if ($(this).closest('li').hasClass('mjs-nestedSortable-collapsed')) {
            $(this).children('i').addClass('glyphicon-plus').removeClass('glyphicon-minus');
        } else {
            $(this).children('i').addClass('glyphicon-minus').removeClass('glyphicon-plus');
        }
    });

    get_data_from_list();
    bindDirtyChecker();
    $('.top-menu input').each(function () {
        $(this).data('original-value', $(this).val());
        $(this).trigger('change');
    });

    $('#new-page-modal form').validate({
        ignore: ".ignore",
        messages: {
            title: 'Sidan måste ha en rubrik',
            page_template: 'Sidan måste ha en mall'
        }
    });

    $('#new-link-modal form').validate({
       ignore: ".ignore",
        messages: {
            title: 'Länken måste ha en rubrik',
            href: 'Länken måste peka någonstans'
        }
    });
});