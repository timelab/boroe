<?php

namespace Timelab\Cms;

use Timelab\Cms\Objects;

interface ModuleInterface {

    /**
     * Gets the name of the admin page, used when routing
     * @return string
     */
    public function getName();

    /**
     * Gets a list of names of all dependant modules
     * @return string[]|null Array with names of all required modules, null if no dependencies
     */
    public function getDependencies();

    /**
     * Gets a list of settings
     * @return \Timelab\Cms\Objects\Setting[] List of all the settings associated with this module
     */
    public function getSettings();

    /**
     * Gets the specified setting.
     * @param $name string The name of the setting
     * @return \Timelab\Cms\Objects\Setting
     */
    public function getSetting($name);

    /**
     * Gets the menu title.
     * @return string
     */
    public function getMenuTitle();

    /**
     * Gets the menu icon.
     * @return string
     */
    public function getMenuIcon();

    /**
     * @return bool True if menu should only appear for administrators, False if it should appear for everyone
     */
    public function isAdminOnly();

    /**
     * Gets the menu order of the module
     * @return int
     */
    public function getMenuOrder();

    /**
     * Routes the request to the module
     */
    public function route();

    /**
     * Called when user is routed to this module, this is done during initialization of wordpress
     * (Good if you want to redirect after action is finished)
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function earlyExecute($action = null, $id = null);

    /**
     * Called when user is routed to this module
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function execute($action = null, $id = null);

    /**
     * Returns the API interface of the module
     * @return ApiInterface
     */
    public function getApi();
}