<?php

namespace Timelab\Cms\Objects;

use Timelab\Cms\ObjectInterface;

class Setting implements ObjectInterface {
    /** @var string $name */
    private $name;

    /** @var string $title */
    private $title;

    /** @var string $description */
    private $description;

    /** @var mixed $value */
    private $value;

    /** @var SettingType $type */
    private $type;

    /**
     * @param $name string The name of the setting
     * @param $title string The display title of the setting
     * @param $description string The description of the setting
     * @param $type string The value type
     */
    public function __construct($name, $title, $description, $type) {
        $this->name = $name;
        $this->title = $title;
        $this->description = $description;
        $this->type = $type;
    }

    /**
     * Gets the name of the setting
     * @return string The internal name of the setting
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the display title of the setting
     * @return string The display name of the setting
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Gets the description of the setting
     * @return string The description of the setting
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Gets the value of the setting
     * @return mixed The value of the setting
     */
    public function getValue()
    {
        $value = get_option($this->getName());

        switch ($this->getType()) {
            case SettingType::BOOL:
                return (strtolower($value) == ('1'||'true')) ? true : false;
                break;
            default:
                return $value;
                break;
        }

    }

    /**
     * Gets the type of the setting
     * @return string The type of the setting
     */
    public function getType()
    {
        return $this->type;
    }
}

abstract class SettingType {
    const STRING    = 'string';
    const HIDDEN    = 'hidden';
    const BOOL      = 'bool';
}