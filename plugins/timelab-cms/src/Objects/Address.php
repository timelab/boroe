<?php

namespace Timelab\Cms\Objects;

use Timelab\Cms\DatabaseObjectAbstract;

class Address extends DatabaseObjectAbstract {

    private $formatted;
    private $street;
    private $zip;
    private $city;
    private $lat;
    private $long;

    /**
     * The post type of the object in the database, used internally by the object when saving
     * @return string The post type of the object
     */
    public function getPostType()
    {
        return "timelab_cms_address";
    }

    /**
     * Checks if user are allowed to save, or only administrators.
     * @return bool `true` if user can save, `false` if only admins can save the data.
     */
    public function canUserSave()
    {
        return true;
    }

    /**
     * Checks if the object is ready to be saved to the database, this is where all the validation lies.
     * @return bool `true` if object can be saved, `false` if not
     */
    public function validateSave()
    {
        return true;
    }


    /**
     * Runs after the loadFromPost method, used to get all custom_fields and other misc data from the database and apply
     * to the object.
     */
    protected function loadFromPostFields()
    {
        $this->setFormatted(get_post_meta($this->getId(), 'formatted', true));
        $this->setStreet(get_post_meta($this->getId(), 'street', true));
        $this->setZip(get_post_meta($this->getId(), 'zip', true));
        $this->setCity(get_post_meta($this->getId(), 'city', true));
        $this->setLat(get_post_meta($this->getId(), 'lat', true));
        $this->setLong(get_post_meta($this->getId(), 'long', true));
    }

    /**
     * Runs after the save method, used to save all custom_fields and other misc data to the database.
     */
    protected function saveFields()
    {
        update_post_meta($this->getId(), 'formatted', $this->getFormatted());
        update_post_meta($this->getId(), 'street', $this->getStreet());
        update_post_meta($this->getId(), 'zip', $this->getZip());
        update_post_meta($this->getId(), 'city', $this->getCity());
        update_post_meta($this->getId(), 'lat', $this->getLat());
        update_post_meta($this->getId(), 'long', $this->getLong());
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getFormatted()
    {
        return $this->formatted;
    }

    /**
     * @param mixed $formatted
     */
    public function setFormatted($formatted)
    {
        $this->formatted = $formatted;
    }

    /**
     * @return mixed
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param mixed $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return mixed
     */
    public function getLong()
    {
        return $this->long;
    }

    /**
     * @param mixed $long
     */
    public function setLong($long)
    {
        $this->long = $long;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @return mixed
     */
    public function getFormattedZip()
    {
        return substr_replace($this->zip, ' ', 3, 0);
    }

    /**
     * @param mixed $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    public function asArray() {
        return array(
            "id"        => $this->getId(),
            "formatted" => $this->getFormatted(),
            "street"    => $this->getStreet(),
            "zip"       => $this->getZip(),
            "city"      => $this->getCity(),
            "lat"       => $this->getLat(),
            "long"      => $this->getLong()
        );
    }

} 