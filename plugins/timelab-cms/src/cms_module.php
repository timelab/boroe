<?php

/**
 * Class cms_module
 * Basklass för en CMS modul, innehåller ett gäng med delade nyttiga funktioner
 */
class cms_module {

    /**
     * Innehåller alla dashboard widgets
     * @var array
     */
    var $dashboard_widgets = array();

    /**
     * Konstruktorn
     */
    public function __construct() {

    }

    /**
     * Kollar om nuvarande användaren är admin
     * @return bool True om användaren är admin, False om inte
     */
    public function is_admin() {
        return current_user_can('manage_options');
    }

    /**
     * Används i en page template för att hämta ut innehållet av en sub_content box och gör den tillgänglig i sideditorn
     * @param $id unika IDet för sub_contetet, används för att döpa custom fieldet
     * @param $name Namnet, detta blir rubriken för editorn
     * @return string Innehållet (om det finns något)
     */
    public function the_sub_content($id, $name) {
        return get_post_meta(get_the_ID(), $id, true);
    }

    /**
     * Hämtar den absoluta urlen för en relativ url.
     * @param $relative_url Den relativa urlen
     * @return string Den absoluta urlen
     */
    public function get_absolute_path($relative_url) {
        trim($relative_url, '/');
        return sprintf("%s/%s", get_site_url(), $relative_url);
    }

    /**
     * Pekar om användaren till url
     * @param $url
     */
    public function redirect($url) {
        header( 'Location: ' . $url );
        die();
    }

    /**
     * Toppen av varje custom admin sida
     */
    public function admin_page_template_top() {
        $slug = get_current_screen()->parent_base;
        echo "<div class='container $slug'>";
        $title = get_admin_page_title();
        echo "<h1>{$title}</h1>";
        if (isset($_GET['message'])) {
            $this->print_message($_GET['message']);
        }
    }

    public function print_message($message) {
        if (empty($message)) {
            return;
        }

        switch ($message) {
            case 'save_success':
                $class = 'alert-success';
                $text = 'Dina ändringar har <b>sparats!</b>';
                break;
            case 'save_failure':
                $class = 'alert-danger';
                $text = 'Dina ändringar <b>gick inte att spara!</b>';
                break;
        }

        echo "<div class='alert $class toast'>$text</div>";
    }


    /**
     * Laddar upp en bild till media mappen
     *
     * @param File $image
     * @return bool|mixed URL till bilden om uppladningen lyckas, False om den misslyckas
     */
    public function handle_image_upload($image) {
        if ($image['error'] == 0) {
            // Hämtar filändelsen
            $extension = strtolower(pathinfo($image['name'], PATHINFO_EXTENSION));

            // Kolla så att filen är en bild
            if (preg_match('/(jpg|jpeg|png|gif)$/', $extension)) {
                $file = wp_handle_upload($image, array( 'test_form' => false ));
                // Ta bort sidurlen från adressen (så den blir relativ)
                return trim($file['url'], '/');
            }
            else {
                // Felaktig extension
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Kör en print_r men omringar den med <pre /> så den ser lite finare ut!
     * @param array $array Objektet som ska loggas
     */
    public function print_r($array) {
        echo "<pre>";
        print_r($array, false);
        echo "</pre>";
    }

    /**
     * Slutet av varje custom admin sida
     */
    public function admin_page_template_bottom() {
        echo "<div class='clear'></div></div>";
    }

    /**
     * Rensar cache (om det finns, gäller endast för pluginet WP Super Cache)
     */
    function clear_cache() {
        $cache_directory = WP_CONTENT_DIR . "/cache";
        if(file_exists($cache_directory)) {
            $this->cleanDir($cache_directory);
        }
    }

    /**
     * Hämtar en inställning från temats config fil (.theme-settings)
     * @param $setting string Inställningen som ska hämtas
     * @param $default string|int|bool Värdet som ska returneras om inställningen inte är satt
     * @return object Returnerar värdet för inställningen i konfig filen, returnerar det specificerade default värdet annars
     */
    function get_theme_setting($setting, $default = null) {
        $config_path = get_template_directory() . "/theme-settings.json";

        if (file_exists($config_path)) {
            $config = file_get_contents($config_path);
            $config = json_decode($config);

            if (isset($config->$setting)) {
                return $config->$setting;
            } else {
                return $default;
            }

        } else {
            return $default;
        }
    }

    /**
     * @param $dirPath
     * @throws InvalidArgumentException
     *
     * Rensar en mapp (OBS! ANVÄND ENDAST I SPECIELLA FALL DÅ DETTA KAN HA HEMSKA KONSEKVENSER!)
     */
    private function cleanDir($dirPath) {
        if (! is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                rmdir($file);
            } else {
                unlink($file);
            }
        }
    }

    /**
     * Pekar om användaren till Timelab CRM inställnings sidan
     */
    function go_to_options_page() {
        $this->redirect( admin_url('admin.php?page=timelab-cms-settings') );
    }
}