<?php
namespace Timelab\Cms\Modules;

use Timelab\Cms\ApiInterface;
use Timelab\Cms\ModuleAbstract;
use phpDocumentor\Reflection\DocBlock;
use Timelab\Cms\Objects\SettingType;

/**
 * Takes care of rendering the options page and also settings for which modules to load
 *
 * Class Options
 * @package Timelab\Cms\Modules
 */
class Options extends ModuleAbstract {

    /**
     * Called on initialization of the Wordpress Admin by hooking into admin_init
     */
    public function onInitialize()
    {
        $this->registerSetting('running_modules', 'Moduler', 'All running Timelab CMS modules', SettingType::HIDDEN);
    }

    /**
     * Gets a list of names of all dependant modules
     * @return string[]|null Array with names of all required modules, null if no dependencies
     */
    public function getDependencies()
    {
        return null;
    }

    /**
     * Returns an array of javascript filenames that should be included when the module is activated.
     * @return string[] Array of javascript filenames to include
     */
    public function jsFiles()
    {
        return array("options-module.js");
    }


    /**
     * Gets the description of the module
     * @return string
     */
    public function getDescription()
    {
        return "Hanterar inställningssidan för CMSet och samtliga moduler";
    }

    public function getMenuTitle()
    {
        return "Moduler";
    }

    /**
     * @return bool True if menu should only appear for administrators, False if it should appear for everyone
     */
    public function isAdminOnly()
    {
        return true;
    }

    /**
     * Gets the menu order of the module
     * @return int
     */
    public function getMenuOrder()
    {
        return 5000;
    }

    public function getMenuIcon()
    {
        return "dashicons-admin-generic";
    }


    /**
     * Called when user is routed to this module, this is done during initialization of wordpress
     * (Good if you want to redirect after action is finished)
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function earlyExecute($action = null, $id = null)
    {
        switch ($action) {
            case 'save':
                $this->saveOptions();
                break;
        }
    }

    /**
     * Called when user is routed to this module
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function execute($action = null, $id = null)
    {
        switch ($action) {
            case null:
                $this->renderList();
                break;
        }
    }

    private function saveOptions() {
        foreach($_POST as $option=>$value) {
            update_option(sanitize_text_field($option), sanitize_text_field($value));
        }

        wp_redirect($this->getUrl());
    }

    /**
     * Renders the list of modules
     */
    private function renderList() {

        $this->loadDocumentationDependencies();

        $parsedown = new \Parsedown();

        $loadedModules = array();

        foreach ($this->getCmsInstance()->getModules() as $module) {

            $class = new \ReflectionClass($module);
            $phpDoc = new DocBlock($class);

            // Get registered settings for module
            $settings = array();

            foreach ($module->getSettings() as $setting) {
                $settings[] = array(
                    "name"          => $setting->getName(),
                    "title"         => $setting->getTitle(),
                    "description"   => $setting->getDescription(),
                    "value"         => $setting->getValue(),
                    "type"          => $setting->getType()
                );
            }

            $loadedModules[] = array(
                "name"              => $module->getName(),
                "description"       => $parsedown->text($phpDoc->getShortDescription()),
                "long_description"  => $parsedown->text($phpDoc->getLongDescription()),
                "adminOnly"         => $module->isAdminOnly(),
                "settings"          => $settings,
                "active"            => true
            );
        }

        $availableModules = array();

        foreach ($this->getCmsInstance()->getAvailableModules() as $module) {
            if ($this->getCmsInstance()->getModule($module->getShortName())) {
                continue;
            }

            $phpDoc = new \phpDocumentor\Reflection\DocBlock($module);

            $availableModules[] = array(
                "name"          => $module->getShortName(),
                "description"   => $parsedown->text($phpDoc->getShortDescription()),
                "api_methods"   => array(),
                "active"        => false
            );
        }

        $data = array(
            'save_url'              => $this->getUrl('save'),
            'activated_modules'     => $loadedModules,
            'available_modules'     => $availableModules
        );

        echo $this->getCmsInstance()->render('Options/ModuleList.twig', $data);
    }

    /**
     * Returns the API interface of the module
     * @return ApiInterface
     */
    public function getApi()
    {
        // TODO: Implement getApi() method.
    }

    public function loadDocumentationDependencies()
    {
        require_once(__DIR__.'/../Vendor/phpDocumentor/Reflection/DocBlock.php');
        require_once(__DIR__.'/../Vendor/phpDocumentor/Reflection/DocBlock/Description.php');
        require_once(__DIR__.'/../Vendor/phpDocumentor/Reflection/DocBlock/Tag.php');
        require_once(__DIR__.'/../Vendor/phpDocumentor/Reflection/DocBlock/Context.php');
        require_once(__DIR__.'/../Vendor/phpDocumentor/Reflection/DocBlock/Location.php');
        require_once(__DIR__.'/../Vendor/phpDocumentor/Reflection/DocBlock/Serializer.php');

        require_once(__DIR__.'/../Vendor/phpDocumentor/Reflection/DocBlock/Type/Collection.php');

        require_once(__DIR__.'/../Vendor/phpDocumentor/Reflection/DocBlock/Tag/ReturnTag.php');
        require_once(__DIR__.'/../Vendor/phpDocumentor/Reflection/DocBlock/Tag/ParamTag.php');
        require_once(__DIR__.'/../Vendor/phpDocumentor/Reflection/DocBlock/Tag/ThrowsTag.php');
        require_once(__DIR__.'/../Vendor/phpDocumentor/Reflection/DocBlock/Tag/SourceTag.php');
        require_once(__DIR__.'/../Vendor/phpDocumentor/Reflection/DocBlock/Tag/ExampleTag.php');

        require_once(__DIR__.'/../Vendor/Parsedown/Parsedown.php');
    }

} 