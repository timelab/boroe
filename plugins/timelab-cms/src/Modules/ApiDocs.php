<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 2014-09-03
 * Time: 10:13
 */

namespace Timelab\Cms\Modules;


use Timelab\Cms\ApiInterface;
use Timelab\Cms\ModuleAbstract;

use phpDocumentor\Reflection\DocBlock;

/**
 * Genererar API dokumentation
 *
 * Class ApiDocs
 * @package Timelab\Cms\Modules
 */
class ApiDocs extends ModuleAbstract {

    private $parsedown;

    /**
     * @return bool True if menu should only appear for administrators, False if it should appear for everyone
     */
    public function isAdminOnly()
    {
        return true;
    }

    public function getMenuTitle()
    {
        return "API Docs";
    }

    public function getMenuIcon()
    {
        return "dashicons-book-alt";
    }

    /**
     * Called on initialization of the Wordpress Admin by hooking into admin_init
     */
    public function onInitialize()
    {
        // TODO: Implement onInitialize() method.
    }

    /**
     * Gets a list of names of all dependant modules
     * @return string[]|null Array with names of all required modules, null if no dependencies
     */
    public function getDependencies()
    {
        return null;
    }

    /**
     * Returns an array of javascript filenames that should be included when the module is activated.
     * @return string[] Array of javascript filenames to include
     */
    public function jsFiles()
    {
        return null;
    }


    /**
     * Gets the menu order of the module
     * @return int
     */
    public function getMenuOrder()
    {
        return 5001;
    }

    /**
     * Called when user is routed to this module, this is done during initialization of wordpress
     * (Good if you want to redirect after action is finished)
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function earlyExecute($action = null, $id = null)
    {
        // TODO: Implement earlyExecute() method.
    }

    /**
     * Called when user is routed to this module
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function execute($action = null, $id = null)
    {
        $this->renderDocumentation();
    }

    private function renderDocumentation() {

        $modules = array();

        $this->getCmsInstance()->getModule('Options')->loadDocumentationDependencies();
        $this->parsedown = new \Parsedown();

        foreach ($this->getCmsInstance()->getModules() as $module) {
            $class = new \ReflectionClass($module);
            $phpDoc = new DocBlock($class);

            $modules[] = array(
                "name"              => $module->getName(),
                "description"       => $this->parsedown->text($phpDoc->getShortDescription()),
                "long_description"  => $this->parsedown->text($phpDoc->getLongDescription()),
                "adminOnly"         => $module->isAdminOnly(),
                "api_methods"       => $this->getApiMethods($module->getApi())
            );
        }

        $objects = array();

        foreach ($this->getCmsInstance()->getAvailableObjects() as $objectClass) {
            $phpDoc = new DocBlock($objectClass);

            $objects[] = array(
                "name"              => $objectClass->getShortName(),
                "description"       => $this->parsedown->text($phpDoc->getShortDescription()),
                "long_description"  => $this->parsedown->text($phpDoc->getLongDescription()),
                "api_methods"       => $this->getApiMethods($objectClass)
            );

        }

        echo $this->getCmsInstance()->render('ApiDocs/Documentation.twig',
            array(
                'modules' => $modules,
                'objects' => $objects
            )
        );
    }

    /**
     * @param $api \ReflectionClass|ApiInterface The class/api to generate api method documentation for
     * @return array
     */
    private function getApiMethods($api) {

        if ($api == null) {
            return;
        }

        $apiMethods = array();

        if (get_class($api) !== 'ReflectionClass') {
            $api = new \ReflectionClass($api);
        }

        // Get all public methods
        foreach ($api->getMethods(\ReflectionMethod::IS_PUBLIC) as $method) {

            $phpDocMethod = new DocBlock($method);

            $paramTags = $phpDocMethod->getTagsByName('param');
            $params = array();


            // Get all params
            foreach($paramTags as $param) {
                $desc = explode(' ', $param->getDescription());
                $type = array_shift($desc);
                $desc = implode(' ', $desc);

                $params[] = array(
                    'name'          => $param->getVariableName(),
                    'description'   => $this->parsedown->text($desc),
                    'type'          => $type
                );

                $param->getName();
            }

            // Get return
            $returnTag = $phpDocMethod->getTagsByName('return');
            $returns = array();

            if (count($returnTag) > 0) {
                $returnTag = $returnTag[0];
                $returns = array(
                    'type'          => $returnTag->getType(),
                    'description'   => $this->parsedown->text($returnTag->getDescription())
                );
            }

            // Get example
            $exampleTag = $phpDocMethod->getTagsByName('example');
            $example = null;

            if (!empty($exampleTag)) {
                $exampleTag = $exampleTag[0];
                $example = $this->parsedown->text($exampleTag->getContent());
            }


            $apiMethods[] = array(
                "name"              => $method->getName(),
                "description"       => $this->parsedown->text($phpDocMethod->getShortDescription()),
                "long_description"  => $this->parsedown->text($phpDocMethod->getLongDescription()),
                "params"            => $params,
                "example"           => $example,
                "returns"           => $returns
            );
        }

        return $apiMethods;
    }

    /**
     * Returns the API interface of the module
     * @return ApiInterface
     */
    public function getApi()
    {
        return null;
    }

} 