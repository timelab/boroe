<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 2014-08-11
 * Time: 10:14
 */

namespace Timelab\Cms\Modules;

require_once('SaveHelperApi.php');

use Timelab\Cms\ModuleAbstract;

/**
 * Module with helper functions related to saving to the WordPress database.
 * Class SaveHelper
 * @package Timelab\Cms\Modules
 */
class SaveHelper extends ModuleAbstract {

    private $api;

    function __construct()
    {
        parent::__construct();
        $this->api = new SaveHelperApi();
    }

    /**
     * Gets a list of names of all dependant modules
     * @return string[]|null Array with names of all required modules, null if no dependencies
     */
    public function getDependencies()
    {
        return null;
    }

    /**
     * Returns an array of javascript filenames that should be included when the module is activated.
     * @return string[] Array of javascript filenames to include
     */
    public function jsFiles()
    {
        return null;
    }


    /**
     * Adds the module to the WordPress menu and links it to the route() function of the module
     */
    public function addToMenu()
    {
        return null; // Don't add this to the menu
    }

    /**
     * @return bool True if menu should only appear for administrators, False if it should appear for everyone
     */
    public function isAdminOnly()
    {
        return true;
    }


    /**
     * Called on initialization of the WordPress Admin by hooking into admin_init
     */
    public function onInitialize()
    {
        // TODO: Implement onInitialize() method.
    }

    /**
     * Called when user is routed to this module, this is done during initialization of wordpress
     * (Good if you want to redirect after action is finished)
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function earlyExecute($action = null, $id = null)
    {
        // TODO: Implement earlyExecute() method.
    }


    /**
     * Called when user is routed to this module
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function execute($action = null, $id = null)
    {
        // TODO: Implement execute() method.
    }

    /**
     * Gets the menu order of the module
     * @return int
     */
    public function getMenuOrder()
    {
        return null;
    }

    /**
     * Returns the API interface of the module
     * @return SaveHelperApi
     */
    public function getApi()
    {
        return $this->api;
    }

} 