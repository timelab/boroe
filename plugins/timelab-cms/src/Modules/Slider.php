<?php
/**
 * Created by PhpStorm.
 * User: Alexander
 * Date: 2014-09-01
 * Time: 10:33
 */

namespace Timelab\Cms\Modules;


use Timelab\Cms\ApiInterface;
use Timelab\Cms\Cms;
use Timelab\Cms\ModuleAbstract;
use Timelab\Cms\Objects\Image;
use Timelab\Cms\Objects\SliderObject;

require_once('SliderApi.php');

/**
 * Enables the slider administration. Admins can create new sliders and Users can modify the content of the sliders
 *
 * Class Slider
 * @package Timelab\Cms\Modules
 */
class Slider extends ModuleAbstract {

    const SLIDER_POST_TYPE = 'timelab_cms_slider';
    const SLIDER_IMAGE_POST_TYPE = 'timelab_cms_image';

    private $api;

    function __construct()
    {
        parent::__construct();
        $this->api = new SliderApi();
    }

    /**
     * @return bool True if menu should only appear for administrators, False if it should appear for everyone
     */
    public function isAdminOnly()
    {
        // TODO: Implement isAdminOnly() method.
    }

    /**
     * Called on initialization of the Wordpress Admin by hooking into admin_init
     */
    public function onInitialize()
    {
        // TODO: Implement onInitialize() method.
    }

    /**
     * Returns an array of javascript filenames that should be included when the module is activated.
     * @return string[] Array of javascript filenames to include
     */
    public function jsFiles()
    {
        return array('slider-module.js');
    }


    /**
     * Gets a list of names of all dependant modules
     * @return string[]|null Array with names of all required modules, null if no dependencies
     */
    public function getDependencies()
    {
        return array('SaveHelper');
    }

    public function getMenuTitle()
    {
        return "Bildspel";
    }

    /**
     * Gets the menu order of the module
     * @return int
     */
    public function getMenuOrder()
    {
        return 20;
    }

    public function getMenuIcon()
    {
        return "dashicons-format-gallery";
    }


    /**
     * Called when user is routed to this module, this is done during initialization of wordpress
     * (Good if you want to redirect after action is finished)
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function earlyExecute($action = null, $id = null)
    {
        switch ($action) {
            case null:
                $this->checkSliders();
                break;
            case 'create':
                $this->createSlider();
                break;
            case 'create_translation':
                $this->createTranslatedSlider($id);
                break;
            case 'save':
                $this->saveSlider($id);
                break;
            case 'delete':
                $this->deleteSlider($id);
                break;
        }
    }

    /**
     * If the user is not an admin and there is only slider, automatically redirect user to the only slider.
     */
    private function checkSliders()
    {
        $sliders = $this->getApi()->getSliders();

        if (!Cms::isSuperAdmin() && count($sliders) === 1) {
            wp_redirect($this->getUrl('edit', $sliders[0]->getId()));
        }
    }

    /**
     * Creates the slider with the specified title
     */
    private function createSlider()
    {
        if (!isset($_POST['title'])) {
            return;
        }

        $slider = new SliderObject();
        $slider->setTitle($_POST['title']);
        $slider->save();

        if ($slider->getId() !== null) {
            wp_redirect($this->getUrl('edit', $slider->getId()));
        } else {
            wp_redirect($this->getUrl());
        }
    }

    private function createTranslatedSlider($id)
    {

        if ($this->getCmsInstance()->getModule('Wpml') !== null && isset($_GET['lang_code'])) {
            /** @var $wpmlApi WpmlApi */
            $wpmlApi = $this->getCmsInstance()->getModule('Wpml')->getApi();
            $slider = new SliderObject();
            $tid = $wpmlApi->translatePost($id, $slider->getPostType(), $_GET['lang_code']);
            wp_redirect($this->getUrl('edit', $tid));

            return;
        }

        wp_redirect($this->getUrl());
    }

    /**
     * Saves the slider
     * @param $id int The ID of the slider to save.
     */
    private function saveSlider($id) {

        /** @var $saveHelper SaveHelper */
        $saveHelper = $this->getCmsInstance()->getModule('SaveHelper');
        $data = $saveHelper->getApi()->bundlePostData($_POST);

        // Save the slider
        $slider = new SliderObject();
        $slider->loadFromPost(get_post($id));

        if (isset($data['title']) && !empty($data['title'])) {
            $slider->setTitle($data['title']);
        }

        $slider->setTitleSupport(isset($data['title_support']));
        $slider->setSubtitleSupport(isset($data['subtitle_support']));

        $slider->save();

        // Save the images
        foreach ($data['objects'] as $imageData) {

            if (!empty($imageData['image_id'])) {
                $image = new Image();
                $image->loadFromPost(get_post($imageData['image_id']));
            } else {
                $image = new Image();
            }

            if (!empty($imageData['image_remove'])) {
                $image->trash();
                continue;
            }

            $image->setParentId($slider->getId());

            $image->setText($imageData['image_text']);

            if (isset($imageData['image_title'])) {
                $image->setTitle($imageData['image_title']);
            }
            if (isset($imageData['image_subtitle'])) {
                $image->setSubtitle($imageData['image_subtitle']);
            }

            $image->setOrder($imageData['image_order']);
            $image->setDateFrom($imageData['image_date_from']);
            $image->setDateTo($imageData['image_date_to']);

            $image->setHref($imageData['image_href']);
            $image->setTarget($imageData['image_target']);

            $image->setMediaId($imageData['media_id']);

            $image->save();
        }

        wp_redirect($this->getUrl('edit', $slider->getId()));
    }

    /**
     * Deletes the specified slider
     * @param $id int The ID of the slider to delete
     * @throws \Exception
     */
    private function deleteSlider($id)
    {
        $slider = new SliderObject();
        $slider->loadFromPost(get_post($id));

        $slider->trash();

        wp_redirect($this->getUrl());
    }


    /**
     * Called when user is routed to this module
     * @param $action string|null The action to be executed
     * @param null $id int The ID of the object to perform the action on
     */
    public function execute($action = null, $id = null)
    {
        switch ($action) {
            case null:
                $this->renderSliderList();
                break;
            case 'edit':
                $this->renderSliderEditor($id);
                break;
        }
    }

    /**
     * Renders the list of Sliders
     */
    private function renderSliderList() {
        $sliders = array();

        foreach ($this->getApi()->getSliders() as $slider) {

            $data = array(
                "id"            => $slider->getId(),
                "title"         => $slider->getTitle(),
                "edit_url"      => $this->getUrl('edit', $slider->getId())
            );

            if ($this->getCmsInstance()->getModule('Wpml')) {
                $data['language'] = $this->getCmsInstance()->getApi('Wpml')->getPostLanguage($slider->getId());
            }

            $sliders[] = $data;
        }

        echo $this->getCmsInstance()->render('Slider/SliderList.twig',
            array(
                'sliders'               => $sliders,
                'create_slider_url'     => $this->getUrl('create')
            )
        );
    }

    /**
     * Renders the slider editor
     * @param $id int The id of the slider to edit
     */
    private function renderSliderEditor($id) {

        $slider = $this->getApi()->getSlider($id, true);
        if ($slider === null) {
            return;
        }

        $languages = array();
        $sliderLanguage = array();
        if ($this->getCmsInstance()->getModule('Wpml') !== null) {
            /** @var $wpmlApi WpmlApi */
            $wpmlApi = $this->getCmsInstance()->getApi('Wpml');

            $languages = $wpmlApi->getTranslationsOfPost($id, $slider->getPostType());

            $sliderLanguage = $wpmlApi->getPostLanguage($id);
        }


        echo $this->getCmsInstance()->render('Slider/SliderEditor.twig',
            array(
                'title'             => $slider->getTitle(),
                'id'                => $slider->getId(),
                'title_support'     => $slider->hasTitleSupport(),
                'subtitle_support'  => $slider->hasSubtitleSupport(),
                'images'            => $slider->getImages(false, true),
                'edit_url'          => $this->getUrl('edit'),
                'save_url'          => $this->getUrl('save', $slider->getId()),
                'delete_url'        => $this->getUrl('delete', $slider->getId()),
                'languages'         => $languages,
                'slider_language'   => $sliderLanguage,
                'translate_url'     => $this->getUrl('create_translation', $slider->getId())
            )
        );
    }

    /**
     * Returns the API interface of the module
     * @return SliderApi
     */
    public function getApi()
    {
        return $this->api;
    }

} 