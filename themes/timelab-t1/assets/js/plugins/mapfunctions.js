$(function(){
	if ( $(window).width() < 1024) {
		   mapOptions = {
			    zoom: 14,
			    streetViewControl: false,
		            disableDefaultUI:true,
		            draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true,
			    center: new google.maps.LatLng(mapLat,mapLong),
			    mapTypeId: google.maps.MapTypeId.ROADMAP
			  };
	}
	else {
	   	  mapOptions = {
	    	    scrollwheel: false,
		    streetViewControl: false,
	            disableDefaultUI:true,
		    zoom: 14,
		    zoomControl: true,
		    center: new google.maps.LatLng(mapLat,mapLong),
		    mapTypeId: google.maps.MapTypeId.ROADMAP
		  };
	}
	
	initialize();

});



function initialize() {

	 var styles = [ 
	  {
            stylers: [
              //{ saturation: -0 },
              //{ gamma: 0.5 },
              //{ weight: 0.5 }
            ]
          },
          {
	    "featureType": "water",
	    "stylers": [
	      //{ "color": "#6e9fd7" }
              //{ saturation: -0 },
              { visibility: 'simplified' }
              //{ gamma: 0.5 },
              //{ weight: 0.5 }
	    ]
	  },{
	  }
	];

	 var styledMap = new google.maps.StyledMapType(styles,{name: "Styled Map"});

	 if(mapLatLongs.length > 0) {
		mapLatLongs.forEach((markerLatLong, index) => {
			responsiveMap = new google.maps.Map(document.getElementById('map-canvas-'+index), mapOptions);

			responsiveMap.mapTypes.set('map_style', styledMap);
   	 		responsiveMap.setMapTypeId('map_style');

			currentBounds = new google.maps.LatLngBounds();

			var image = baseUrl + '/assets/img/map-marker.png';
 	 		var myLatLng = new google.maps.LatLng(markerLatLong.lat,markerLatLong.long);
 	 		beachMarker = new google.maps.Marker({position: myLatLng,map: responsiveMap,icon: image});
			currentBounds.extend(myLatLng);

			// Don't zoom in too far on only one marker
			if (currentBounds.getNorthEast().equals(currentBounds.getSouthWest())) {
				var extendPoint1 = new google.maps.LatLng(currentBounds.getNorthEast().lat() + 0.005, currentBounds.getNorthEast().lng() + 0.005);
				var extendPoint2 = new google.maps.LatLng(currentBounds.getNorthEast().lat() - 0.005, currentBounds.getNorthEast().lng() - 0.005);
				currentBounds.extend(extendPoint1);
				currentBounds.extend(extendPoint2);
			 }

			responsiveMap.fitBounds(currentBounds);

			responsiveMaps.push({map: responsiveMap, bound: currentBounds });
		});
	 } else {
		responsiveMap = new google.maps.Map(document.getElementById('map-canvas-1'), mapOptions);

		responsiveMap.mapTypes.set('map_style', styledMap);
   	 	responsiveMap.setMapTypeId('map_style');

	 	var image = baseUrl + '/assets/img/map-marker.png';
 	 	var myLatLng = new google.maps.LatLng(mapLat,mapLong);
 	 	beachMarker = new google.maps.Marker({position: myLatLng,map: responsiveMap,icon: image});
		bounds.extend(beachMarker);
		responsiveMap.fitBounds(bounds);
	 }

 	 
 	 //Start centercheck
 	setMapcenter();
 	 
}

var mapTimer;
function setMapcenter(){
	$(window).resize(function() {
		window.clearTimeout(mapTimer);
		mapTimer = window.setTimeout(function(){
			responsiveMaps.forEach(responsiveMap => {
				responsiveMap.map.fitBounds(responsiveMap.bound);
			});
		},200);	
	});
}




