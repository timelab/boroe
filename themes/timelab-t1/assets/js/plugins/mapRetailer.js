var searchPostBound = "";
var circleBounds = "";
var isSearch = false;
var CompBound = new Array();
var timer;
var markerArr = new Array(0);
var prodArr = new Array();

$(function(){

  if ( $(window).width() < 1024) {
       mapOptions = {
          zoom: 5,
          streetViewControl: false,
          disableDefaultUI:true,
          draggable: true, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true,
          center: new google.maps.LatLng(63, 17),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        };
  }
  else {
        mapOptions = {
        scrollwheel: false,
        streetViewControl: false,
        disableDefaultUI:true,
        zoom: 5,
        zoomControl: true,
        center: new google.maps.LatLng(63, 17),
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
  }

  var map = new google.maps.Map(document.getElementById('map'), mapOptions);

  var infowindow = new google.maps.InfoWindow();

  var marker, i;

  for (var i = 0; i < locations.length; i++) {

    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[i][1], locations[i][2]),
      map: map
    });

    markerArr.push(marker);

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
      return function() {
        infowindow.setContent("<strong>"+locations[i][0]+"</strong><br />"+locations[i][3]+"<br />"+locations[i][4]+" "+locations[i][5]+"<br /><br /><strong>Telefon:</strong> "+locations[i][6]);
        infowindow.open(map, marker);
      }
    })(marker, i));
  };



  $('.map-container .search-retailer').on('keyup change', '.post-nmb[type=text], input[type=checkbox]', function(){
    var zip = $('.map-container .post-nmb').val();
    //CompBound = []; 
      clearOverlays();
      prodArr = new Array();
      CompBound = Array();

      if(zip == ""){
        isSearch = false;
        rebuild();
        createComp();
      }
      else{
        isSearch = true;
        if(zip.indexOf(' ') < 0){
          zipArr = zip.split("");
          zip = "";
          for(var i = 0; i < 5; i++){
            if(i == 3){
              zip = zip+" "+zipArr[i];
              continue;
            }
            zip = zip+zipArr[i];
          }
        }else{
          zipArr = zip.split(' ');
          zip = "";
          for(var i = 0; i < 5; i++){
            zip = zip+zipArr[i];
          }
          zipArr = zip.split("");
          zip = "";
          for(var i = 0; i < 5; i++){
            if(i == 3){
              zip = zip+" "+zipArr[i];
              continue;
            }
            zip = zip+zipArr[i];
          }
        }

        $.ajax({
            url: "https://maps.googleapis.com/maps/api/geocode/json?components=postal_code:"+zip+"|country:sweden&sensor=true&key=AIzaSyBTJqTd2lmiuCF2kc-s5iZIyAnAswQpquQ",
            type: "GET",
            dataType: 'JSON',
            success: function(data){
              if(data.status != "ZERO_RESULTS"){
                searchPostBound = data.results[0].geometry.bounds;
                createMarkers();
                createComp();
              }
            }
        });
      }
  });

  $( ".map-container form" ).submit(function( event ) {
      event.preventDefault();
    /*if(circleBounds == ""){
      var zip = $('input[name=zip]', this).val();

      if(zip.indexOf(' ') < 0){
        zipArr = zip.split("");
        zip = "";
        for(var i = 0; i < 5; i++){
          if(i == 3){
            zip = zip+" "+zipArr[i];
            continue;
          }
          zip = zip+zipArr[i];
        }
      }

      $.ajax({
          url: "https://maps.googleapis.com/maps/api/geocode/json?components=postal_code:"+zip+"|country:sweden&sensor=true",
          type: "GET",
          dataType: 'JSON',
          success: function(data){
            searchPostBound = data.results[0].geometry.bounds;
            circleBounds = circleBound();

            $('<input />').attr('type', 'hidden')
                      .attr('name', "ne")
                      .attr('value', circleBounds.O.O+","+circleBounds.O.j)
                      .appendTo('.map-container form');
            $('<input />').attr('type', 'hidden')
                      .attr('name', "sw")
                      .attr('value', circleBounds.j.O+","+circleBounds.j.j)
                      .appendTo('.map-container form');
           $('.map-container form').submit();
          }
      });
    }*/

  });
  
  function createComp(){
    var url = $(".retailers").data('ajax-url');

    $.ajax({
        url: url,
        type: "GET",
        data: {allowed: CompBound, search: isSearch, products: prodArr},
        success: function(data){
          $(".retailers").html(data);
        }
    });
  }

  function createMarkers(){
    var bounds = new google.maps.LatLngBounds(searchPostBound['southwest'], searchPostBound['northeast']);

    var dummyMarker = new google.maps.Marker({
        position: new google.maps.LatLng(searchPostBound['southwest'], searchPostBound['northeast']),
        visible: false,
        map: map,
    });


    var circle = new google.maps.Circle({
              map: map,
              radius: 50000, // 50 km
              fillOpacity: 0,
              strokeOpacity: 0,
            });

    circle.bindTo('center', dummyMarker, 'position');
            map.fitBounds(circle.getBounds());

    circleBounds = circle.getBounds();

    var checkedNum = $('.map-container input[type=checkbox]:checked').length;

    if($('.map-container input[name="heatingproducts"]').is(':checked')){
      prodArr.push('Heating products');
    }

    if($('.map-container input[name="trinette"]').is(':checked')){
      prodArr.push('Trinette');
    }

    if($('.map-container input[name="webshop"]').is(':checked')){
      prodArr.push('Webshop');
    }

    for (var i = 0; i < locations.length; i++) {
      if (circle.getBounds().contains(new google.maps.LatLng(locations[i][1], locations[i][2]))){
        if(!checkedNum){
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
          });

          markerArr.push(marker);

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent("<strong>"+locations[i][0]+"</strong><br />"+locations[i][3]+"<br />"+locations[i][4]+" "+locations[i][5]+"<br /><br /><strong>Telefon:</strong> "+locations[i][6]);
              infowindow.open(map, marker);
            }
          })(marker, i));

          CompBound.push(locations[i][0]);
        }
        else{
          if(($('.map-container input[name="heatingproducts"]').is(':checked') && (locations[i][7][0] == 1)) || ($('.map-container input[name="trinette"]').is(':checked') && (locations[i][7][1] == 1)) || ($('.map-container input[name="webshop"]').is(':checked') && (locations[i][7][2] == 1))){
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(locations[i][1], locations[i][2]),
              map: map
            });

            markerArr.push(marker);

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
              return function() {
                infowindow.setContent("<strong>"+locations[i][0]+"</strong><br />"+locations[i][3]+"<br />"+locations[i][4]+" "+locations[i][5]+"<br /><br /><strong>Telefon:</strong> "+locations[i][6]);
                infowindow.open(map, marker);
              }
            })(marker, i));

            CompBound.push(locations[i][0]);
          }
        }
      }
    };
  }

  function clearOverlays() {
    for (var i = 0; i < markerArr.length; i++ ) {
      markerArr[i].setMap(null);
    }
    markerArr.length = 0;
  }

  function rebuild(){
    markerArr = new Array(0);

    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    infowindow = new google.maps.InfoWindow();

    marker, i;

    var checkedNum = $('.map-container input[type=checkbox]:checked').length;

    if($('.map-container input[name="heatingproducts"]').is(':checked')){
      prodArr.push('Heating products');
    }

    if($('.map-container input[name="trinette"]').is(':checked')){
      prodArr.push('Trinette');
    }

    if($('.map-container input[name="webshop"]').is(':checked')){
      prodArr.push('Webshop');
    }

    for (var i = 0; i < locations.length; i++) {
      if(!checkedNum){
        marker = new google.maps.Marker({
          position: new google.maps.LatLng(locations[i][1], locations[i][2]),
          map: map
        });

        markerArr.push(marker);

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            infowindow.setContent("<strong>"+locations[i][0]+"</strong><br />"+locations[i][3]+"<br />"+locations[i][4]+" "+locations[i][5]+"<br /><br /><strong>Telefon:</strong> "+locations[i][6]);
            infowindow.open(map, marker);
          }
        })(marker, i));
      }
      else{
        if(($('.map-container input[name="heatingproducts"]').is(':checked') && (locations[i][7][0] == 1)) || ($('.map-container input[name="trinette"]').is(':checked') && (locations[i][7][1] == 1)) || ($('.map-container input[name="webshop"]').is(':checked') && (locations[i][7][2] == 1))){
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
          });

          markerArr.push(marker);

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent("<strong>"+locations[i][0]+"</strong><br />"+locations[i][3]+"<br />"+locations[i][4]+" "+locations[i][5]+"<br /><br /><strong>Telefon:</strong> "+locations[i][6]);
              infowindow.open(map, marker);
            }
          })(marker, i));

          CompBound.push(locations[i][0]);
        }
      }
    };
  }
});

