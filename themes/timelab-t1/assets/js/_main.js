// Modified http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
// Only fires on body class (working off strictly WordPress body_class)

var ExampleSite = {
  // All pages
  common: {
    init: function() {
      // JS here


        window.setSideBarHeight = function() {
            if ($(window).width() > 991) {
                $('.subBorderLeft').css('min-height',$('.row-eq-height').height());
            } else {
                $('.subBorderLeft').css('min-height','auto');
            }
        }

        setSideBarHeight();

        $(window).on('resize', function () {
            setSideBarHeight();
        });


        $('.mailerlite-form-field input').addClass('form-control');
        $('.mailerlite-form input[type="submit"]').addClass('btn').addClass('btn-primary');


        $('.wpcf7-form-control-wrap .wpcf7-form-control').each(function(){
            $(this).focus(function(){
                $(this).parent().find('span').fadeOut();
            });
        });

        $('.woocommerce-product-search .search-button').click(function(){
            $(this).parent().submit();
        });

        //Lägg till en clone av huvudmenyval när det finns dropdowns
        //Loopa alla a med klassen dropdown-toggle
        $('.navbar-nav a.dropdown-toggle').each(function(){
            //Clona first-child i submeny (Borde alltid finnas åtminstone en eftersom det är en dropdown)
            $cloned = $('ul li:first-child',$(this).parent()).clone();

            //Ta klasserna från huvudmenyval men exkludera dropdownklassen
            var $classes = $(this).parent().attr('class').replace('dropdown','');

            //Om det finns en annan submeny som är aktiv, ta bort även active classen som kommer från huvudmenyval då en submeny är active
            $classes = $(this).parent().find('ul li.active').length > 0 ? $classes.replace('active', ''):$classes;

            //Modifiera classer,href och text på clonen för att spegla huvudmenyval
            $cloned.attr('class',$classes).find('a').attr('href',$(this).attr('href')).text($(this).text());

            //Prependa den så den hamnar först i submenyn
            $('ul',$(this).parent()).prepend($cloned);
        });

        $(window).on('resize', function(){
          if ( $(window).width() > 394) {
            var select = $('.retailer-template .retailer .adress .small-header');
            select.parent().siblings('.contact').removeAttr('style');
            select.siblings('.hidden-small').removeAttr('style');
          }
        });


        $('.retailer-template .retailers').on('click', '.retailer .adress .small-header', function(){
          if ( $(window).width() < 394) {
            $(this).parent().siblings('.contact').slideToggle('fast');
            $(this).siblings('.hidden-small').slideToggle('fast');
          }
        });

    },
    finalize: function() { }
  },
  // Home page
  home: {
    init: function() {
      // JS here
    }
  },
  // About page
  about: {
    init: function() {
      // JS here
    }
  }
};

var UTIL = {
  fire: function(func, funcname, args) {
    var namespace = ExampleSite;
    funcname = (funcname === undefined) ? 'init' : funcname;
    if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
      namespace[func][funcname](args);
    }
  },
  loadEvents: function() {

    UTIL.fire('common');

    $.each(document.body.className.replace(/-/g, '_').split(/\s+/),function(i,classnm) {
      UTIL.fire(classnm);
    });

    UTIL.fire('common', 'finalize');
  }
};

$(document).ready(UTIL.loadEvents);
