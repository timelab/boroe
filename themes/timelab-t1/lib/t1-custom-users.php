<?php
/**
 * Created by PhpStorm.
 * User: Peter
 * Date: 2015-11-06
 * Time: 13:28
 */

//New role on theme activation
add_action('after_switch_theme', 'mytheme_setup_options');
function mytheme_setup_options () {
    $result = add_role(
        'reseller_installer',
        __( 'Reseller/Installer','t1' ),
        array(
            'read'         => true,  // true allows this capability
            'edit_posts'   => true,
            'delete_posts' => false, // Use false to explicitly deny
        )
    );
}


add_action('show_user_profile', 'my_add_extra_profile_fields');
add_action( 'edit_user_profile', 'my_add_extra_profile_fields' );
function my_add_extra_profile_fields($user)
{
    if ($user->has_cap('reseller_installer')) {
    ?>
        <script type="text/javascript">
            var upload_image_frame;
            $(function(){

                $('h3:first').next().hide();
                $('h3:first').hide();

                $('h3:contains("Customer")').each(function(){
                    $(this).next().hide();
                    $(this).hide();
                });


                //Custom bild
                upload_image_frame = create_media_frame("Lägg in bild", "Lägg till", add_user_image, false);

                function add_user_image(image){
                    $('#company-img').attr('src',image.url);
                    $('#t1-companyimage').val(image.id);
                }

                $('#new-company-image').click(function() {
                    upload_image_frame.open($(this));
                });


            });
        </script>
        <h3><?php _e('Company information','t1'); ?></h3>
        <table class="form-table">

            <tr>
                <th><label><?php _e('Image','t1'); ?></label></th>
                <td>
                    <?php
                        $companyimage = "";
                        if(esc_attr(get_the_author_meta( 't1-companyimage', $user->ID ) ) !=''){
                            $companyimage = wp_get_attachment_image_src( esc_attr(get_the_author_meta( 't1-companyimage', $user->ID ) ), 'full')[0];
                        }
                    ?>
                    <img src="<?php echo $companyimage; ?>" id="company-img" style="max-width:400px;height:auto;">
                    <input type="hidden" id="t1-companyimage" name="t1-companyimage" value="<?php echo esc_attr(get_the_author_meta( 't1-companyimage', $user->ID ) ); ?>" />
                </td>
            </tr>
            <tr>
                <th></th>
                <td><div class="btn btn-primary" id="new-company-image"><?php _e('Choose image','t1'); ?></div></td>
            </tr>

            <tr>
                <th><label><?php _e('Type of company','t1'); ?></label></th>
                <td>
                    <input type="hidden" name="t1-installer" value="0" />
                    <input type="hidden" name="t1-reseller" value="0" />
                    <input type="checkbox" name="t1-installer" id="t1-installer" value="1" <?php echo esc_attr(get_the_author_meta( 't1-installer', $user->ID ) != '1' ? '': 'checked' ); ?>/> <?php _e('Installer','t1'); ?><br />
                    <input type="checkbox" name="t1-reseller" id="t1-reseller" value="1" <?php echo esc_attr(get_the_author_meta( 't1-reseller', $user->ID ) != '1' ? '': 'checked' ); ?>/> <?php _e('Reseller','t1'); ?>
                </td>
            </tr>


            <tr>
                <th><label><?php _e('Type of products','t1'); ?></label></th>
                <td>
                    <input type="hidden" name="t1-heatingproducts" value="0" />
                    <input type="hidden" name="t1-trinette" value="0" />
                    <input type="hidden" name="t1-webshop" value="0" />
                    <input type="checkbox" name="t1-heatingproducts" id="t1-heatingproducts" value="1" <?php echo esc_attr(get_the_author_meta( 't1-heatingproducts', $user->ID ) != '1' ? '': 'checked' ); ?>/> <?php _e('Heating products','t1'); ?><br />
                    <input type="checkbox" name="t1-trinette" id="t1-trinette" value="1" <?php echo esc_attr(get_the_author_meta( 't1-trinette', $user->ID ) != '1' ? '': 'checked' ); ?>/> <?php _e('Trinette','t1'); ?><br />
                    <input type="checkbox" name="t1-webshop" id="t1-webshop" value="1" <?php echo esc_attr(get_the_author_meta( 't1-webshop', $user->ID ) != '1' ? '': 'checked' ); ?>/> <?php _e('Webshop','t1'); ?>
                </td>
            </tr>


            <tr>
                <th><label for="t1-companyname"><?php _e('Name','t1'); ?></label></th>
                <td><input type="text" name="t1-companyname" id="t1-companyname" value="<?php echo esc_attr(get_the_author_meta( 't1-companyname', $user->ID ) ); ?>" class="regular-text" /></td>
            </tr>
            <tr>
                <th><label><?php _e('Search and autopopulate fields','t1'); ?></label></th>
                <td>
                    <input type="hidden" name="address_id[]" value="{{ id }}">
                    <div class="row">
                        <div class="col-sm-7">
                            <input type="search" class="form-control input-lg transient" name="address_search" placeholder="<?php _e('Ex. &quot;Kungsgatan 34, Luleå&quot;','t1'); ?>">
                        </div>

                        <div class="col-sm-5">
                            <a class="btn btn-primary btn-lg btn-block address-search-button"><span class="glyphicon glyphicon-search"></span> <?php _e('Search address','t1'); ?></a>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <th><label for="t1-companyaddress"><?php _e('Address','t1'); ?></label></th>
                <td><input type="text" name="t1-address_street" id="t1-address_street" value="<?php echo esc_attr(get_the_author_meta( 't1-address_street', $user->ID ) ); ?>" class="regular-text" /></td>
            </tr>
            <tr>
                <th><label for="t1-address_zip"><?php _e('Zip','t1'); ?></label></th>
                <td><input type="text" name="t1-address_zip" id="t1-address_zip" value="<?php echo esc_attr(get_the_author_meta( 't1-address_zip', $user->ID ) ); ?>" class="regular-text" /></td>
            </tr>
            <tr>
                <th><label for="t1-address_city"><?php _e('City','t1'); ?></label></th>
                <td><input type="text" name="t1-address_city" id="t1-address_city" value="<?php echo esc_attr(get_the_author_meta( 't1-address_city', $user->ID ) ); ?>" class="regular-text" /></td>
            </tr>
            <tr>
                <th><label for="t1-address_lat"><?php _e('Latitude','t1'); ?></label></th>
                <td><input type="text" name="t1-address_lat" id="t1-address_lat" value="<?php echo esc_attr(get_the_author_meta( 't1-address_lat', $user->ID ) ); ?>" class="regular-text" /></td>
            </tr>
            <tr>
                <th><label for="t1-address_long"><?php _e('Longitude','t1'); ?></label></th>
                <td><input type="text" name="t1-address_long" id="t1-address_long" value="<?php echo esc_attr(get_the_author_meta( 't1-address_long', $user->ID ) ); ?>" class="regular-text" /></td>
            </tr>
            <tr>
                <th><label><?php _e('Map','t1'); ?></label></th>
                <td><div id="address-map-container" style="height:300px"></div></td>
            </tr>
            <tr>
                <th><label for="t1-companyphone"><?php _e('Phone','t1'); ?></label></th>
                <td><input type="text" name="t1-companyphone" id="t1-companyphone" value="<?php echo esc_attr(get_the_author_meta( 't1-companyphone', $user->ID ) ); ?>" class="regular-text" /></td>
            </tr>
            <tr>
                <th><label for="t1-companyfax"><?php _e('Fax','t1'); ?></label></th>
                <td><input type="text" name="t1-companyfax" id="t1-companyfax" value="<?php echo esc_attr(get_the_author_meta( 't1-companyfax', $user->ID ) ); ?>" class="regular-text" /></td>
            </tr>
            <tr>
                <th><label for="t1-companyemail"><?php _e('E-mail','t1'); ?></label></th>
                <td><input type="text" name="t1-companyemail" id="t1-companyemail" value="<?php echo esc_attr(get_the_author_meta( 't1-companyemail', $user->ID ) ); ?>" class="regular-text" /></td>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <th><label for="t1-companydesc"><?php _e('Description','t1'); ?></label></th>
                <td><textarea name="t1-companydesc" id="t1-companydesc"  rows="5" cols="30" /><?php echo esc_attr(get_the_author_meta( 't1-companydesc', $user->ID ) ); ?></textarea></td>
            </tr>
        </table>
    <?php
    }
}


add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );
function my_save_extra_profile_fields( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;

    //Find all t1-* fields and update DB
    $regex = "/^(t1-*)/";
    foreach($_POST as $name=>$value) {
        if(preg_match($regex, $name)) {
            update_user_meta($user_id,$name, $_POST[$name]);
        }
    }

}



