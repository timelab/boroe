<?php

global $show_categories;
$show_categories = true;


// Rerdirect to home if landing on startpage for shop
function redirect_homepage() {
    global $post;
    if($post->post_name === 'shop'){
      $home = get_page_by_path(ICL_LANGUAGE_CODE == 'en' ? 'home':'hem');
      wp_redirect(get_permalink($home->ID));
    }
}
add_action( 'wp', 'redirect_homepage',100000 );




/* Woocommerce specific filters / changes */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}


add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
function jk_woocommerce_breadcrumbs() {
    return array(
        'delimiter'   => ' <span class="bredcrumb-delimiter">-</span> ',
        'wrap_before' => '',
        'wrap_after'  => '',
        'before'      => '',
        'after'       => '',
    );
}



//Category editor
// Add term page
add_action( 'product_cat_add_form_fields', 'wpm_taxonomy_add_new_meta_field', 10, 2 );
function wpm_taxonomy_add_new_meta_field() {
    // this will add the custom meta field to the add new term page
    ?>
    <div class="form-field">
        <label for="term_meta[custom_term_meta]"><?php _e( 'Details', 'wpm' ); ?></label>
        <textarea name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" rows="5" cols="40"></textarea>
        <p class="description"><?php _e( 'Detailed category info to appear below the product list','wpm' ); ?></p>
    </div>
<?php
}


// Edit term page
add_action( 'product_cat_edit_form_fields', 'wpm_taxonomy_edit_meta_field', 10, 2 );
function wpm_taxonomy_edit_meta_field($term) {

    // put the term ID into a variable
    $t_id = $term->term_id;

    // retrieve the existing value(s) for this meta field. This returns an array
    $term_meta = get_option( "taxonomy_$t_id" );
    $content = $term_meta['custom_term_meta'] ? wp_kses_post( $term_meta['custom_term_meta'] ) : '';
    $settings = array( 'textarea_name' => 'term_meta[custom_term_meta]' );
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Details', 'wpm' ); ?></label></th>
        <td>
            <?php wp_editor( $content, 'product_cat_details', $settings ); ?>
            <p class="description"><?php _e( 'Detailed category info to appear below the product list','wpm' ); ?></p>
        </td>
    </tr>
<?php
}

// Save extra taxonomy fields callback function
add_action( 'edited_product_cat', 'save_taxonomy_custom_meta', 10, 2 );
add_action( 'create_product_cat', 'save_taxonomy_custom_meta', 10, 2 );
function save_taxonomy_custom_meta( $term_id ) {
    if ( isset( $_POST['term_meta'] ) ) {
        $t_id = $term_id;
        $term_meta = get_option( "taxonomy_$t_id" );
        $cat_keys = array_keys( $_POST['term_meta'] );
        foreach ( $cat_keys as $key ) {
            if ( isset ( $_POST['term_meta'][$key] ) ) {
                $term_meta[$key] = wp_kses_post( stripslashes($_POST['term_meta'][$key]) );
            }
        }
        // Save the option array.
        update_option( "taxonomy_$t_id", $term_meta );
    }
}

//Redirect för kategorier om de saknar beskrivning
//Går till första barnkategorin eller till första produkt om barnkategori också saknas
if(!is_admin()){
    add_action('parse_query', 'redirect_empty_cat');
}
function redirect_empty_cat( $wp_query ) {
    $t_id = get_queried_object()->term_id;
    if($t_id != null){

        $term_meta = get_option( "taxonomy_$t_id" );
        $term_meta_content = $term_meta['custom_term_meta'];
        global $show_categories;

        if ( $term_meta_content == '' && !$show_categories ) {

            //$category_children = get_term_children($t_id,'product_cat');

            $child_args = array(
                'parent' => $t_id
            );
            $category_children = get_terms('product_cat', $child_args);


            $permalink = '';
            if(!empty($category_children)){
                $permalink  = get_term_link((int)$category_children[0]->term_id,'product_cat');
            }

            global $wpdb;
            $prod_id = $wpdb->get_var("SELECT wpp.ID FROM wp_posts wpp, wp_term_relationships wptr WHERE wptr.term_taxonomy_id = " . $t_id . " AND wptr.object_id = wpp.ID and wpp.post_status = 'publish' order by wpp.menu_order asc LIMIT 1");

            if($prod_id != null){
                $permalink = get_permalink($prod_id);
            }

            header('Location: '.$permalink);
            die();
        }
    }
}


// Display details on product category archive pages
add_action( 'woocommerce_before_shop_loop', 'wpm_product_cat_archive_add_meta' );
function wpm_product_cat_archive_add_meta() {

    global $show_categories;


    $t_id = get_queried_object()->term_id;
    $term_meta = get_option( "taxonomy_$t_id" );
    $term_meta_content = $term_meta['custom_term_meta'];
    if ( $term_meta_content != '' ) {
        $show_categories = false;
        echo '<div class="woo-sc-box normal rounded full">';
        echo apply_filters( 'the_content', $term_meta_content );
        echo '</div>';
    } else {

    }
}


//Remove tabs
function woo_remove_product_tabs( $tabs ) {
    unset( $tabs['reviews'] ); 			// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab
    return $tabs;
}
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

//Remove related products
function wc_remove_related_products( $args ) {
    return array();
}
add_filter('woocommerce_related_products_args','wc_remove_related_products', 10);

//Wrap list image
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);


if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
    function woocommerce_template_loop_product_thumbnail() {
        echo woocommerce_get_product_thumbnail();
    }
}
if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {
    function woocommerce_get_product_thumbnail( $size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0  ) {
        global $post, $woocommerce;
        if ( has_post_thumbnail() ) {
        $output = '<div class="imagewrapper">';

            $output .= get_the_post_thumbnail( $post->ID, $size );

        $output .= '</div>';
        }
        return $output;
    }
}

/*
* Byta av bild saknas
*
**/
add_action( 'init', 'custom_fix_thumbnail' );
function custom_fix_thumbnail() {
	add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');

	function custom_woocommerce_placeholder_img_src( $src ) {
		$src = get_template_directory_uri().'/assets/img/bild_saknas.jpg';
		return $src;
	}
}



//Products per page, 12 as standard
$productsPerPage = isset($_GET["perpage"]) ? ($_GET["perpage"] == 'alla' ? -1 : $_GET["perpage"]) : 12;
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return '. $productsPerPage .';' ), 20 );



//New widget area for the webshop
register_sidebar(
	array (
		'name' => 'Shop Sidebar'
	)
);


//Read more link whereever it is used
function new_excerpt_more( $more ) {
	return '... <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Läs mer</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

function woocommerce_store_thumbnail($size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0) {
	global $post;
	$product = wc_get_product($post->ID);

	$images = $product->get_gallery_attachment_ids();

	if (!empty($images)) {
		echo wp_get_attachment_image( $images[0], apply_filters( $size, 'shop_catalog' ) );
	} else {
		// PLACEHOLDER HÄR
	}


}



if ( ! class_exists( 'MY_Product_Cat_List_Walker' ) ) :

	class MY_Product_Cat_List_Walker extends Walker {

		public $tree_type = 'product_cat';
		public $db_fields = array ( 'parent' => 'parent', 'id' => 'term_id', 'slug' => 'slug' );

		/**
		 * @see Walker::start_lvl()
		 * @since 2.1.0
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param int $depth Depth of category. Used for tab indentation.
		 * @param array $args Will only append content if style argument value is 'list'.
		 */
		public function start_lvl( &$output, $depth = 0, $args = array() ) {
			if ( 'list' != $args['style'] )
				return;

			$indent = str_repeat("\t", $depth);
			$output .= "$indent<ul class='children'>\n";
		}

		/**
		 * @see Walker::end_lvl()
		 * @since 2.1.0
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param int $depth Depth of category. Used for tab indentation.
		 * @param array $args Will only append content if style argument value is 'list'.
		 */
		public function end_lvl( &$output, $depth = 0, $args = array() ) {
			if ( 'list' != $args['style'] )
				return;

			$indent = str_repeat("\t", $depth);
			$output .= "$indent</ul>\n";
		}

		/**
		 * @see Walker::start_el()
		 * @since 2.1.0
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param int $depth Depth of category in reference to parents.
		 * @param integer $current_object_id
		 */
		public function start_el( &$output, $cat, $depth = 0, $args = array(), $current_object_id = 0 ) {

            $output .= '<li class="cat-item cat-item-' . $cat->term_id;

            $children = get_term_children($cat->term_id,'product_cat');

			if(is_array($children) && count($children) > 1 && $depth > 0){
				$output .= ' cat-has-children';
			}




			if ( $args['current_category'] == $cat->term_id ) {
				$output .= ' current-cat';
			}

			if ( $args['has_children'] && $args['hierarchical'] ) {
				$output .= ' cat-parent';
			}

			if ( $args['current_category_ancestors'] && $args['current_category'] && in_array( $cat->term_id, $args['current_category_ancestors'] ) ) {
				$output .= ' current-cat-parent';
			}

            /*
            if($depth > 1) {
                //var_dump($depth);
                //var_dump($cat->category_count);
                if($cat->category_count == 1) {
                    //var_dump($cat->name);
                    // Everything in here needs to be linked to their only product
                    // Find the child then get the link
                    $child_products = wc_get_products(array(
                        'category' => array($cat->slug),
                    ));
                    var_dump($child_products[0]->get_name());
                    $output .=  '"><a href="' . $child_products[0]->get_permalink() . '">' . __( $cat->name, 'woocommerce' ) .'</a>';
                } else {
                    $output .=  '"><a href="' . get_term_link( (int) $cat->term_id, $this->tree_type ) . '">' . __( $cat->name, 'woocommerce' ) .'</a>';
                }
                //var_dump($children);
            } else {
                $output .=  '"><a href="' . get_term_link( (int) $cat->term_id, $this->tree_type ) . '">' . __( $cat->name, 'woocommerce' ) .'</a>';
            }
            */

            $output .=  '"><a href="' . get_term_link( (int) $cat->term_id, $this->tree_type ) . '">' . __( $cat->name, 'woocommerce' ) .'</a>';

            if(is_archive()){
                $current_product = 0;
            }else{
                $current_product = get_the_ID();
            }

            if (empty($children) && $args['current_category'] == $cat->term_id) {

                $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 99,
                    'product_cat' => $cat->slug,
                    'orderby' => 'menu_order',
                    'order' => 'ASC'
                );


                $products = new WP_Query($args);
                wp_reset_postdata();

            }


			if ( $args['show_count'] ) {
				$output .= ' <span class="count">(' . $cat->count . ')</span>';
			}

            if(isset($products) && $products->have_posts()){
                if(count($products->posts) !== 1) {
                    $output .= "<ul class='product-children'>";
                    while ( $products->have_posts() ) {
                        $products->the_post();
                        $class = $current_product == $products->post->ID ? 'class="current-product"':'';
                        $output .= '<li ' . $class . '><a href="'. get_permalink($products->post->ID) .'">' . get_the_title( $products->post->ID ) . '</a></li>';

                    }

                    $output .= "</ul>";
                }
            }
            unset($products);


		}

		/**
		 * @see Walker::end_el()
		 * @since 2.1.0
		 *
		 * @param string $output Passed by reference. Used to append additional content.
		 * @param int $depth Depth of category. Not used.
		 * @param array $args Only uses 'list' for whether should append to output.
		 */
		public function end_el( &$output, $cat, $depth = 0, $args = array() ) {

			$output .= "</li>\n";
		}

		/**
		 * Traverse elements to create list from elements.
		 *
		 * Display one element if the element doesn't have any children otherwise,
		 * display the element and its children. Will only traverse up to the max
		 * depth and no ignore elements under that depth. It is possible to set the
		 * max depth to include all depths, see walk() method.
		 *
		 * This method shouldn't be called directly, use the walk() method instead.
		 *
		 * @since 2.5.0
		 *
		 * @param object $element Data object
		 * @param array $children_elements List of elements to continue traversing.
		 * @param int $max_depth Max depth to traverse.
		 * @param int $depth Depth of current element.
		 * @param array $args
		 * @param string $output Passed by reference. Used to append additional content.
		 * @return null Null on failure with no changes to parameters.
		 */
		public function display_element( $element, &$children_elements, $max_depth, $depth = 0, $args, &$output ) {
			if ( ! $element || 0 === $element->count ) {
				return;
			}
			parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
		}
	}

endif;


add_filter('woocommerce_product_categories_widget_args', 'timelab_product_categories_widget_args', 10, 1);

function timelab_product_categories_widget_args($args) {
	$args['walker'] = new My_Product_Cat_List_Walker;
	return $args;
}

/* Redirect if there is only one product in the category or tag, or anywhere... */

function redirect_to_single_post(){
    global $wp_query;
    if( (is_product_category() || is_product_tag()) && $wp_query->post_count == 1 )//Woo single only
    //if( is_archive() && $wp_query->post_count == 1 )//redirect all single posts
    {
        the_post();
        $post_url = get_permalink();
        wp_safe_redirect($post_url , 302 );
    exit;
    }
}
add_action('template_redirect', 'redirect_to_single_post');


?>
