<?php


function hide_update_notice_to_all_but_admin_users()
{
    if (!current_user_can('manage_woocommerce')) {
        wp_enqueue_style('timelabt1', get_template_directory_uri() . '/admin-hide.css', false, '1.3');
    }
}
add_action( 'admin_head', 'hide_update_notice_to_all_but_admin_users', 1 );


//Walher for subpage menus
class My_Sub_Walker extends Walker
{
    public function walk( $elements, $max_depth, ...$args)
    {
        global $isParent;
        $list = array ();
        $parent = $isParent;


        foreach ( $elements as $item )
        {
            if($parent == 0){
                //Sök topförälder
                foreach($item->classes as $class){
                    if($class == 'current-menu-item' || $class == 'current-menu-parent'){
                        $parent = $item->ID;
                    }
                }
            } else {
                if($item->menu_item_parent == $parent){
                    $list[] = "<li><a href='$item->url' class='" . str_replace('current-menu-item','active',implode(' ',$item->classes)) . "'>$item->title</a></li>";
                } else {

                }
            }
        }
        return join( "\n", $list );
    }
}

class JC_Walker_Nav_Menu extends Walker_Nav_Menu {

    var $menu_id = 0;
    var $menu_items = array();

    function __construct($id = 0){
        $this->menu_id = $id;
        $this->menu_items[] = $id;
    }


    function start_el( &$output, $item, $depth = 0, $args = [], $id = 0 ) {
        if( !empty($this->menu_items) && !in_array($item->ID, $this->menu_items) && !in_array($item->menu_item_parent, $this->menu_items)){
            return false;
        }

        if(!in_array($item->ID, $this->menu_items)){
            $this->menu_items[] = $item->ID;
        }

        //On submeny get title from page instead of menu
        $page = get_page_by_path(str_replace(get_site_url(),'',$item->url));
        $item->title = $page->post_title;


        parent::start_el($output, $item, $depth, $args);
    }

    function end_el( &$output, $item, $depth = 0, $args = array() ) {
        if( !empty($this->menu_items) && !in_array($item->ID, $this->menu_items) && !in_array($item->menu_item_parent, $this->menu_items)){
            return false;
        }
        parent::end_el($output, $item, $depth, $args);
    }

}


function emitAAA(){
    return "<div class='soliditet-aaa'>
                <a href='https://www.soliditet.se/lang/sv_SE/RatingGuideline' target='_blank' style='text-decoration: none;'><img style='border:0px;' oncontextmenu='return false' title='Vi &auml;r ett kreditv&auml;rdigt f&ouml;retag enligt Bisnodes v&auml;rderingssystem som baserar sig p&aring; en m&auml;ngd olika beslutsregler. Denna uppgift &auml;r alltid aktuell&#44; informationen uppdateras dagligen via Bisnodes databas.' alt='Vi &auml;r ett kreditv&auml;rdigt f&ouml;retag enligt Bisnodes v&auml;rderingssystem som baserar sig p&aring; en m&auml;ngd olika beslutsregler. Denna uppgift &auml;r alltid aktuell&#44; informationen uppdateras dagligen via Bisnodes databas.' id='img_98_58_px' src='https://merit.soliditet.se/merit/imageGenerator/display?lang=SE&country=SE&cId=weg9xinx1dDtlMu3tzfMqw%3D%3D&cUid=avyjqvTOPxM%3D&imgType=img_98_58_px' /></a>
        </div>";
}


function emitBrands($cms)
{
	global $t1config;
	$html = "";
	
	if ($t1config['brands_enabled'] === false)
	{
		return $html;
	}
	
	$podparams = array('where' => 'synlig.meta_value=True', 'orderby' => 'ordning.meta_value ASC,id DESC', 'limit' => 6);
	$objectList = pods('brands', $podparams);
	if ($objectList != null && $objectList->total() > 0)
	{
		$html .= "<div class='container showcases brands PodsPuffar {$t1config['hide_brands']}'>";
			$html .= "<div class='row'>";

			$classes = "";
			switch ($objectList->total())
			{
				case 1:
				case 2:
				case 3:
					$classes = "col-xs-4 col-sm-4 col-md-4";
					break;
				case 4:
					$classes = "col-xs-6 col-sm-3 col-md-3";
					break;
				case 5:
				case 6:
					$classes = "col-xs-6 col-sm-2 col-md-2";
					break;
				default:
					$classes = "col-xs-6 col-sm-2 col-md-2";
			}
			$border = "border";

			$i = 0;
			while ($objectList->fetch())
			{
				$i++;
				if ($i === $objectList->total()) { $border = ""; }

				$linkArr = $objectList->field('puff_lank');

				$html .= ($linkArr !== "") ? "<a href='{$linkArr}'>" : "";
				$html .= "<span class='{$classes} {$border} brand'>";
					$html .= "<span class='brand-bg'>";
						$html .= '<img src="' . $objectList->display('puff_bild') . '"/>';
						
						if (!empty($objectList->display('puff_bild_hover')))
						{
							$html .= '<img class="hover" src="' . $objectList->display('puff_bild_hover') . '"/>';
						}
					$html .= '</span>';
				$html .= '</span>';
				$html .= ($linkArr !== "") ? '</a>' : '';
			}
			$html .= "</div>";
		$html .= "</div>";
	}

	return $html;
}


function emitShowcases($cms)
{
	$html = "";

	$podparams = array('where' => 'synlig.meta_value=True', 'orderby' => 'ordning.meta_value ASC,id DESC', 'limit' => 4);
	$objectList = pods('puffar', $podparams);
	if ($objectList->total() > 0)
	{
		$count = 0;

		$html .= "<div class='container showcases PodsPuffar'>";
			$html .= "<div class='row showcase-row'>";
			while ($objectList->fetch())
			{
				$count++;
				$linkArr = $objectList->field('puff_lank');
				$linkCatArr = $objectList->field('puff_link_product_category');

				if($linkCatArr) {
					$podpermalink = get_term_link((int)$linkCatArr['term_id'], 'product_cat' );
				} else {
					$podpermalink = get_permalink($linkArr['ID']);
				}

                $html .= "<a href='$podpermalink'>";
					$html .= "<span class='col-md-3 col-sm-6 col-xs-6 showcase pos$count'>";
						$html .= "<span class='showcase-bg'>";
							$html .= "<img src='{$objectList->display('puff_bild')}' alt='{$objectList->field('puff_text')}'/>";
							if (!empty($objectList->display('puff_bild_hover')))
							{
								$html .= "<img class='hover' src='{$objectList->display('puff_bild_hover')}' alt='{$objectList->field('puff_text_hover')}'/>";
							}
							$html .= "<span class='showcase-title'>{$objectList->field('puff_text_rubrik')}</span>";
							if ($objectList->field('puff_text') !== "")
							{
								$html .= "<span class='showcase-text'>{$objectList->field('puff_text')}</span>";
							}
						$html .= "</span>";
					$html . "</span>";
				$html .= "</a>";
			}
			$html .= "</div>";
		$html .= "</div>";
	}

	return $html;
}

function emitShowcases_vertical($cms)
{
	$podparams = array('where' => 'synlig.meta_value=True', 'orderby' => 'ordning.meta_value ASC,id DESC', 'limit' => 4);
	$objectList = pods('puffar', $podparams);

    $html = '<div class="showcases showcases-vertical">';
    if ($objectList->total() > 0)
    {
		// loop through items using pods::fetch
		$count = 0;
		while ($objectList->fetch())
    {
			$count++;
			$linkArr = $objectList->field('puff_lank');
			$podpermalink = get_permalink($linkArr['ID']);

			$html .= "<div class='col-xs-6 col-sm-6 col-md-12 showcase showcase-vertical pos$count'>";
				$html .= "<a href='$podpermalink' class='showcase-href'>";
					$html .= '<span class="showcase-bg">';
						$html .= '<img src="'.$objectList->display('puff_bild').'" alt="'.$objectList->field('puff_text').'" />';
						if (!empty($objectList->display('puff_bild_hover')))
						{
							$html .= "<img class='hover' src='{$objectList->display('puff_bild_hover')}' alt='{$objectList->field('puff_text_hover')}'/>";
						}
						$html .= "<span class='showcase-title'>{$objectList->field('puff_text_rubrik')}</span>";
						if ($objectList->field('puff_text') !== "")
						{
							$html .= "<span class='showcase-text'>{$objectList->field('puff_text')}</span>";
						}
					$html .= '</span>';
				$html .='</a>';
			$html .= '</div>';
		}
	}
    $html .='</div>';
    return $html;
}


function emitSlider($cms, $sliderID)
{
	$html = "<div class='sliderShadow'></div>";
	$html .= "<div class='flexslider'>";
		$html .= "<ul class='slides'>";

        /** @var $slider Timelab\Cms\Objects\SliderObject */
        $slider = $cms->getApi('Slider')->getSlider($sliderID);

		foreach ($slider->getImages(true) as $slide)
		{
            $title      = $slide->getTitle();
            $subtitle   = $slide->getSubtitle();
            $text       = $slide->getText();

            $slideHtml = "";

            if ( !(empty($title) && empty($subtitle) && empty($text))  )
            {
                $slideHtml .= "<div class='slideText'>";

                    $href       = $slide->getHref();

                    $slideHtml .= empty($title) ? "" : "<h2>{$title}</h2>";
                    $slideHtml .= empty($subtitle) ? "" : "<h3>{$subtitle}</h3>";
                    $slideHtml .= empty($text) ? "" : "<p>{$text}</p>";
                $slideHtml .= "</div>";
            }


            $slideHtml .= "<img src='{$slide->getSrc()}'/>";

			$html .=  "<li>";
				$html .= empty($href) ? $slideHtml : "<a href='{$href}' target='{$slide->getTarget()}'>$slideHtml</a>";
			$html .= "</li>";
		}
		$html .= "</ul>";
	$html .= "</div>";
	
	return $html;
}


function emitSocial($cms)
{
	global $t1config;
	$html = "";
	
	if ($t1config['social_integration_enabled'] === false)
	{
		return $html;
	}
	
	$html .= "<div>";

        if (!empty($t1config['href_twitter']))
        {
            $html .= "<a href='{$t1config['href_twitter']}' target='_blank'>";
            $html .= "<img class='instagramicon' src='" . get_template_directory_uri() . "/assets/img/top_instagram.png' alt='Instagram'/>";
            $html .= "</a>";
        }
		if (!empty($t1config['href_facebook']))
		{
			$html .= "<a href='{$t1config['href_facebook']}' target='_blank'>";
				$html .= "<img class='facebookicon' src='" . get_template_directory_uri() . "/assets/img/top_facebook.png' alt='Facebook'/>";
			$html .= "</a>";
		}

	$html .= "</div>";
	
	return $html;
}


function getContactInfo($cms)
{
	// Loop through facilities to gather data.
	$facilities = array();
	foreach ($cms->getApi('Contact')->getFacilities() as $facility)
	{	
		$address = $facility->getAddress();
	
		$data = array();
		$data['address'] = $address->getStreet() . ' ' . $address->getZip() . ' ' . $address->getCity();
		$data['address_2'] = $address->getStreet() . ' ' . $address->getCity();
		foreach ($facility->getContactDetails() as $fUppgift)
		{
			if ($fUppgift->getValue() !== '')
			{
				$data[$fUppgift->getType()] = $fUppgift->getValue();
			}
		}
		$facilities[] = $data;
	}
	
	return $facilities;
}

function emitRetailers($user, $allowed = null, $search = false){

  $name = (esc_attr(get_the_author_meta( 't1-companyname', $user->ID ) ) == null) ? $user->display_name : esc_attr(get_the_author_meta( 't1-companyname', $user->ID ) );

  $found = true;

  if(($allowed != null) || ($search == 'true')){
    if($allowed != null){
      foreach ($allowed as $allowedComp) {
        if($allowedComp != $name){
          $found = false;
        }
        else{
          $found = true;
          break;
        }
      }
    }
    else{
      $found = false;
    }
  }

  if($found){
  $comp = $name;
  $lat = esc_attr(get_the_author_meta( 't1-address_lat', $user->ID ) );
  $long = esc_attr(get_the_author_meta( 't1-address_long', $user->ID ) );
  $address = esc_attr(get_the_author_meta( 't1-address_street', $user->ID ) );
  $zip = esc_attr(get_the_author_meta( 't1-address_zip', $user->ID ) );
  $city = esc_attr(get_the_author_meta( 't1-address_city', $user->ID ) );
  $phone = esc_attr(get_the_author_meta( 't1-companyphone', $user->ID ) );
  $epost = esc_attr(get_the_author_meta( 't1-companyemail', $user->ID ) );

  $ret_html .= "<div class='retailer'>";
  $ret_html .= "<div class='row'>";
  $ret_html .= "<div class='col-xs-6 adress'>";
  $ret_html .= "<span class='small-header'><strong>" . $comp . "</strong><i class='fa fa-caret-down'></i></span><br />";
  $ret_html .= "<div class='hidden-small small-addrs'>";
  $ret_html .= $address . "<br />";
  $ret_html .= $zip . " " .  $city;
  $ret_html .= "</div>";
  $ret_html .= "</div>";
  $ret_html .= "<div class='col-xs-6 hidden-small contact'>";
  $ret_html .= "<strong>Tel: </strong>" . $phone . "<br />";
  $ret_html .= "<a href='" . $user->data->user_url . "'>Hemsida</a><br />";
  $ret_html .= "<a href='mailto:" . $epost . "'>E-post</a>";
  $ret_html .= "</div>";
  $ret_html .= "</div>";
  $ret_html .= "</div>";
  }

  return $ret_html;
}
