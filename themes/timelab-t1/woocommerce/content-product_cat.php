<?php
/**
 * The template for displaying product category thumbnails within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product_cat.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     4.7.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce_loop;

// Store loop count we're currently on
if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;

// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );

// Increase loop count
$woocommerce_loop['loop']++;

global $show_categories;

if($show_categories){

?>
<div class="product-category product col-xs-6 col-sm-3">

	<?php
		if($category->category_count == 1) {
			do_action( 'woocommerce_before_shop_loop_item' );
		} else {
			do_action( 'woocommerce_before_subcategory', $category );
		}
	?>
	<?php
		if($category->category_count == 1) {
			$products = wc_get_products(array(
				'category' => array($category->slug),
			));
		}
	?>
	<a href="<?php
		if($category->category_count == 1) {
			echo $products[0]->get_permalink();
		} else {
			echo get_term_link( $category->slug, 'product_cat' );
		}
	?>">

		<?php
			/**
			 * woocommerce_before_subcategory_title hook
			 *
			 * @hooked woocommerce_subcategory_thumbnail - 10
			 */

			/*
			if($category->category_count == 1) {
				?>
				<div class="imagewrapper">
				<?php
					echo $products[0]->get_image();
				?>
				</div>
				<?php
			} else {
			*/
			do_action( 'woocommerce_before_subcategory_title', $category );
			// }
		?>

		<h3>
			<?php
				if($category->category_count == 1) {
					echo $products[0]->get_name();
				} else {
					echo $category->name;
				}
			?>
		</h3>
		<div class="exerpt">
			<?php
				if($category->category_count == 1) {
					echo $products[0]->get_short_description();
				} else {
					echo $category->category_description;
				}
			?>
		</div>

		<?php
			/**
			 * woocommerce_after_subcategory_title hook
			 */
			if($category->category_count == 1) {
				do_action( 'woocommerce_after_shop_loop_item_title' );
			} else {
				do_action( 'woocommerce_after_subcategory_title', $category );
			}
		?>

	</a>

	<?php
		if($category->category_count == 1) {
			//do_action( 'woocommerce_after_shop_loop_item_title' );
		} else {
			do_action( 'woocommerce_after_subcategory', $category );
		}
	?>

</div>
<?php } ?>