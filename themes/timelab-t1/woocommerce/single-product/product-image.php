<?php
/**
 * Single Product Image
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.5.1
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $woocommerce, $product;

?>
<div class="images">

	<?php

        $attachment_count = count( $product->get_gallery_attachment_ids() );
        if($attachment_count > 0){
            //var_dump($product->product_image_gallery);
            $image_id = explode(',',$product->product_image_gallery);

            $image_title = esc_attr( get_the_title( $image_id[0] ) );
            $image_link  = wp_get_attachment_url( $image_id[0] );



            $image = wp_get_attachment_image($image_id[0],'full',false,array(
                'title' => $image_title
            ));


            echo $image;
            //echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s" data-rel="prettyPhoto' . $gallery . '">%s</a>', $image_link, $image_title, $image ), $post->ID );


        } else {
            echo "<img src='" . get_template_directory_uri() . "/assets/img/bild_saknas.jpg' alt='" . __('Image missing','t1') . "' />";
        }




	?>

	<?php //do_action( 'woocommerce_product_thumbnails' ); ?>

</div>
