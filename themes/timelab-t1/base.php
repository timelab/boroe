<?php

    global $cms;
    global $t1config;
    get_template_part('templates/head');

    $settings = pods('facilities');
		global $facility;
		$idArr = (ICL_LANGUAGE_CODE == 'en') ? explode(',',$settings->field('english_facility_id')) : explode(',',$settings->field('swedish_facility_id'));
        $idArr = (ICL_LANGUAGE_CODE == 'en') ? explode(',',$settings->field('english_facility_id')) : explode(',',$settings->field('swedish_facility_id'));

        global $facilityArr;

        foreach($idArr as $value){
            $facilityArr[] = $cms->getApi('Contact')->getFacility($value);
        }

        $facility = $cms->getApi('Contact')->getFacility($facilityArr[0]->getID());

?>

<body <?php body_class(); ?>>
	<div class="container">
		<?php 
			get_template_part('templates/header');
	
			do_action('get_header');
			// Use Bootstrap's navbar if enabled in config.php
	     	if (current_theme_supports('bootstrap-top-navbar')) 
			{
	        	get_template_part('templates/header-top-navbar');
	      	} 
	      	else 
			{
	        	echo "<h2>Warning! Please enable the bootstrap navbar in config.php</h2>";
	      	}
	    ?>
	</div>  
	<?php include roots_template_path(); ?>
  	<?php get_template_part('templates/footer'); ?>
</body>
</html>
