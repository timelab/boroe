<?php
	global $cms;
	global $t1config;
	$footer = '';
	$repArr = array ("(0)", "-", " "); // array för att fixa till telefonnr i länkar.

	global $facility;

	$footerHeader = '';$footerText='';

	$address = $facility->getAddress();

	$footer .= "<div class='background'><img src='". get_template_directory_uri() ."/assets/img/karta_ny.png'></div>";

	$footer .= "<div class='row'>";


	//addressen

	if ($address) {
		$footer .= "<div class='col-sm-3 col-md-3 col-lg-3 footer-address'>";
		$footer .= "<div class='kontaktHeadline'>Borö Pannan AB</div>";
		$footer .= "<div class='kontakt underline'>" . $address->getStreet() . ", " . $address->getCity() . "</div>";
		$footer .= "<div class='kontakt underline'>" . $facility->getEmail()->getFormatted() . "</div>";
		$footer .= "<div class='kontakt underlineMobile'>" . $facility->getTelephone()->getFormatted() . "</div>";
		$footer .= "</div>";
	}

    $footer .= "<div class='col-sm-3 col-md-3 col-lg-3 footer-links'>";
    $footer .= "<div class='kontaktHeadline'>" . __("Links","t1"). "</div>";

    $defaults = array(
        'theme_location'  => '',
        'menu'            => 16,
        'container'       => 'div',
        'container_class' => 'footer-menu-container',
        'container_id'    => '',
        'menu_class'      => 'menu',
        'menu_id'         => 'footer-menu',
        'echo'            => false,
        'fallback_cb'     => false,
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'depth'           => 1
    );

    $footer .= wp_nav_menu( $defaults );
    
    $footer .= "</div>";


    $footer .= "<div class='col-sm-3 col-md-6 col-lg-6'>";
        $footer .= "<div class='kontaktHeadline'>Borö Worldwide</div>";
            $defaults = array(
                'theme_location'  => 'footer_word_wide',
                'container'       => 'div',
                'container_class' => 'footer-menu-container',
                'container_id'    => '',
                'menu_class'      => 'worldwide',
                'menu_id'         => '',
                'echo'            => false,
                'fallback_cb'     => false,
                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth'           => 1
            );
        
            $footer .= wp_nav_menu( $defaults );
            // $footer .= "<ul class='worldwide'>";
            //     // global $facilityArr;
            //     // $contact_url = ICL_LANGUAGE_CODE == 'sv' ? 'kontakt' : 'en/contact';
            //     // foreach($facilityArr as $fac){
            //     //     $footer .= "<li><a href='" . get_site_url() . "/" . $contact_url . "/?a=" . $fac->getID() . "'>" . $fac->getAddress()->getCity() . "</a></li>";
            //     // }
            //     $footer .= "<li><a href='" . get_site_url() . "/om-oss/'>". __('Motala','t1') ."</a></li>";
            //     $footer .= "<li><a href='" . get_site_url() . "/esc-doo-bosnien/'>". __('Bosnia','t1') ."</a></li>";
            // $footer .= "</ul>";
        $footer .= "</div>";
    $footer .= "</div>";
	
?>


<footer class="content-info container footer <?php echo $t1config['footer_type']; ?>" role="contentinfo">
	<div class="footer-container">
		<?php echo $footer; ?>
	</div>
</footer>

<?php wp_footer(); ?>