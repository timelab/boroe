<?php
	global $cms;
	global $t1config;
?>

<div class="row top-header">		
	<div class="col-xs-12 col-sm-6 col-md-7 col-lg-6 logo-mobile">
		<a href="<?= site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/boroe_new.png" alt="Logotype" class="topLogo"/></a>
	</div>

	<div class="hidden-xs col-sm-6 col-md-5 col-lg-6 text-right">
        <div class="row">
            <div class="col-sm-12">
                <a href="http://issuu.com/boroe" target="_blank" class="social-top issuu" title="<?php _e('Boroe on Issuu','t1'); ?>"><i class="fa fa-dot-circle-o"></i></a>
                <a href="https://se.pinterest.com/boropannan/" target="_blank" class="social-top pintrest" title="<?php _e('Boroe on Pintrest','t1'); ?>"><i class="fa fa-pinterest"></i></a>
                <a href="https://www.instagram.com/explore/locations/1008440635/" target="_blank" class="social-top instagram" title="<?php _e('Boroe on Instagram','t1'); ?>"><i class="fa fa-instagram"></i></a>
                <a href="https://twitter.com/Boroepannan" target="_blank" class="social-top twitter" title="<?php _e('Boroe on Twitter','t1'); ?>"><i class="fa fa-twitter"></i></a>
                <a href="https://www.linkedin.com/company/bor-ab" target="_blank" class="social-top linkedin" title="<?php _e('Boroe on LinkedIn','t1'); ?>"><i class="fa fa-linkedin"></i></a>
                <a href="https://www.facebook.com/boroepannan/" target="_blank" class="social-top facebook" title="<?php _e('Boroe on Facebook','t1'); ?>"><i class="fa fa-facebook"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form role="search" method="get" class="woocommerce-product-search" action="<?php echo home_url(); ?>">
                    <input type="search" class="search-field" placeholder="<?php _e('Search products','t1'); ?>" value="" name="s" title="<?php _e('Search for:','t1'); ?>"><a class="search-button"><i class="fa fa-search"></i></a>
                    <input type="hidden" name="post_type" value="product">
                </form>
            </div>
        </div>
    </div>
</div>