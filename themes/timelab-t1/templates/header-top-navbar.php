<div class="row">
	<header class="banner navbar navbar-inverse" role="banner">
		<div class="navbar-header">
	      	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        	<span class="sr-only">Toggle navigation</span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	      	</button>
	      	<a class="navbar-brand" href="<?php echo home_url(); ?>/"><?php bloginfo('name'); ?></a>

	      	<div class="language-container only-xs">
    				<!--<?php do_action('icl_language_selector'); ?>-->
    		</div>
	    </div>
	
	    <nav class="collapse navbar-collapse" role="navigation">
      	<div class="language-container hidden-xs">
    			<!--<?php do_action('icl_language_selector'); ?>-->
    		</div>
				<?php
	      	if (has_nav_menu('primary_navigation'))
	      	{
	        		wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
	      	}
	    	?>
	    </nav>
	</header>
</div>
