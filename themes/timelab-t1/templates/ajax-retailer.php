<?php
require_once('../../../../wp/wp-blog-header.php');
require_once('../lib/t1-lib.php');

if(isset($_GET['allowed'])){
  $allowed = $_GET['allowed'];
}

if(isset($_GET['search'])){
  $search = $_GET['search'];
}

if(isset($_GET['products'])){
  $products = $_GET['products'];
}
else{
  $products = array();
}

$args = array(
  'role' => 'reseller_installer'
);

$user_query = new WP_User_Query( $args );

$ret_html = "";
$count = 0;

if ( ! empty( $user_query->results ) ) {
  foreach ( $user_query->results as $user ) {
    $response = emitRetailers($user, $allowed, $search);
    $ret_html .= $response;
    if($response != null){
      $count++;
    }
  }
}

if(isset($_GET['search']) && (isset($_GET['allowed']))){
  if(count($products) >= 1){
    $prod_html;
    $count = 0;

    foreach ($products as $product) {
      $count++;

      if((count($products) > 1) && (count($products) > $count)){
        $prod_html .= __($product,'t1').", ";
      }
      else{
        $prod_html .= __($product,'t1')." ";
      }
    }

    $title_html = "<div class='retailers-title'>";
    $title_html .= "<h2><span class='hittade'>" . $count . " företag hittades - </span><span class='typ'>Återförsäljare " . $prod_html . "</span></h2>";
    $title_html .= "</div>";
  }
  else{
    $title_html = "<div class='retailers-title'>";
    $title_html .= "<h2><span class='hittade'>" . $count . " företag hittades</h2>";
    $title_html .= "</div>";
  }
}
else if($count == 0){
  $title_html = "<div class='retailers-title'>";
  $title_html .= "<h2><span class='hittade'>" . $count . " företag hittades</h2>";
  $title_html .= "</div>";
}
else{
  $title_html = "";
}
$assemble = $title_html;
$assemble .= $ret_html;

echo $assemble;
?>