<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<title><?php wp_title('|', true, 'right'); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">
	<link rel="apple-touch-icon" sizes="57x57" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/favicon-194x194.png" sizes="194x194">
	<link rel="icon" type="image/png" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/android-chrome-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/manifest.json">
	<link rel="mask-icon" href="<?= get_template_directory_uri(); ?>/assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#da532c">
	<meta name="msapplication-TileImage" content="<?= get_template_directory_uri(); ?>/assets/img/favicon/mstile-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<script type="text/javascript">
		var baseUrl = '<?php echo get_template_directory_uri(); ?>';
	</script>
	<?php
		// All header info from WP and the template.
		wp_head();
	
		// Custom fonts from the config file.
		global $t1config;
		foreach ($t1config['fonts'] as $font) 
		{
			echo "<link href='{$font}' rel='stylesheet' type='text/css'>";
		}
	?>
	
	<link rel="alternate" type="application/rss+xml" title="<?php echo get_bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
	
	<?php
	    //Special include only on affected pages.
	    global $wp_query;
	    $toReplace = array(".php","page-templates/");
	    $template_name = str_replace($toReplace, "", get_post_meta($wp_query->post->ID, '_wp_page_template', true));
	    
	    if (preg_match("#template-startsida(-nyheter)?#", $template_name))
		{
	        echo "<script src='" . get_template_directory_uri() . "/assets/js/plugins/jquery.flexslider.js'></script>\n";

	    }
	    
	    

	    // Map and form API scripts coontact page.
	    if($template_name == "template-kontakt")
		{
	        echo "<script src=\"https://maps.googleapis.com/maps/api/js?v=3.14&sensor=false&key=AIzaSyAcBsr9K9Gm-89onZSJisvCKdX7b2HGHMY\"></script>\n";
	        echo "<script>
			mapLat = 65.586784;
			mapLong = 22.153072;
			var responsiveMap;
			var beachMarker;
		</script>\n";
	        echo "<script src='" . get_template_directory_uri() . "/assets/js/plugins/mapfunctions.js'></script>\n";
	        echo "<script src='" . get_template_directory_uri() . "/assets/js/plugins/formfunctions.js'></script>\n";
	    }

	        // Map API script retailer page.
	        if($template_name == "template-retailer")
	    	{
	            echo "<script src=\"https://maps.googleapis.com/maps/api/js?v=3.14&sensor=false&key=AIzaSyAcBsr9K9Gm-89onZSJisvCKdX7b2HGHMY\"></script>\n";
	            echo "<script src='" . get_template_directory_uri() . "/assets/js/plugins/mapRetailer.js'></script>\n";
	        }
	?>
</head>
