<?php
/**
 *	Template Name: Kontaktsida Mall
 */
$site_url = get_site_url();
?>







<?php
global $cms;
$tabs = '';

$panels = '';
$active = 'active';
$facilities = $cms->getApi('Contact')->getFacilities();
$jscript = '';
$repArr = array("(0)", "-", " "); //array för att fixa till telefonnr i länkar.
$latlongs = array();
$staffHtml = '';

/** @var $facility Timelab\Cms\Objects\Facility */



foreach ($facilityArr as $facility)
{
	$address = $facility->getAddress();
	$openinghours = $facility->getOpeningHours();
	$contactDetails = $facility->getContactDetails();

    $latlongs[] = array(
        'lat' => $address->getLat(),
        'long' => $address->getLong()
    );

	$staffHtml .='<div class="staff-list">';

    foreach ($facility->getDepartments() as $departmentName => $department){

        $staffHtml .= "<div class='clear'></div><div class='col-xs-12 department'><h3>" . $departmentName . "</h3></div>\n";
        foreach ($department as $person)
        {
            $staffHtml .= '<div class="col-xs-12 col-md-4 col-sm-6 personal">'."\n";
            $image = $person->getImage();
            if(isset($image)){
                $staffHtml .= "<img src='" . $image->getSrc() . "' alt='{$person->getName()}' />\n";
            } else {
                $staffHtml .= "<img src='/assets/img/personalbild-saknas.jpg' />\n";
            }

            $staffHtml .= "<div class='PL_infosquare'>\n";
            $staffHtml .= "<div class='PL_name'>{$person->getName()}</div>\n";
            $staffHtml .= "<div class='PL_role'>{$person->getRole()}</div>\n";
            $staffHtml .= "<div class='PL_description'>{$person->getDescription()}</div>\n";

            $pUppgifter = $person->getContactDetails();
            foreach ($pUppgifter as $pUppgift)
            {
                if ($pUppgift->getValue() !== '')
                {
                    $staffHtml .= "<div class='PL_Uppgift PL_{$pUppgift->getType()}'>";
                    if ($pUppgift->getType() === 'email')
                    {
                        $staffHtml .= "<a href='mailto:{$pUppgift->getValue()}'>E-post</a>\n";
                    }
                    else
                    {
                        $staffHtml .= "<span class='key'>Tel: </span>";
                        $staffHtml .= $pUppgift->getFormatted();
                    }
                    $staffHtml .= "</div>";
                }
            }
            $staffHtml .= "</div><!-- /infosquare -->\n";
            $staffHtml .= "</div><!-- /personal -->\n";
        }



    }

	$staffHtml .= "</div>";


	//Kontaktinformation
	$contact ='<div class="row contact-column">';
	$contact .= '<div class="col-xs-12 visible-xs col-sm-12 visible-sm hidden-md hidden-lg">';
	$contact .= '<hr>';
	$contact .= '</div>';
	
    $contact .= "<div class='clear'></div>";

    $contact .= '<div class="col-xs-12 col-sm-12 col-md-12 contactInfo">';
    $contact .= '<h3>' . __('Contact form','t1') . '</h3>';
    $contact .= do_shortcode('[contact-form-7 id="' . ((ICL_LANGUAGE_CODE == 'sv') ? 4 : 239) . '"]');
    $contact .= emitAAA();
    $contact .= '</div>';
    $contact .= "</div>\n";

    $contact .='<div class="row contact-column" >';
    $contact .= '<div class="col-xs-12 col-sm-12 col-md-12">';
    $contact .= '<hr style="margin-top: 0;">';
    $contact .= '</div>';
    $contact .= "<div class='clear'></div>";
    $contact .= "</div>\n";

    $contact .='<div class="row contact-column">';
    $contact .= '<div class="col-xs-12 col-sm-12 col-md-12 sidebar-page-text">';
    $contact .= get_the_content();
    $contact .= '</div>';
    $contact .= "<div class='clear'></div>";
    $contact .= "</div>\n";



	//Öppettider
	if (count($openinghours) > 0)
	{
        $contact .='<div class="row contact-column">';
		$contact .= '<div class="col-xs-12 col-sm-12 col-md-12">';
		$contact .= '<hr>';
		$contact .= '</div>';

		$contact .='<div class="col-xs-12 col-sm-6 col-md-12 showcaseX contactInfo">';
		$contact .= "<h3>Öppettider</h3>\n";
		$contact .= "<div class='oppettiderfix'>\n";
		foreach($openinghours as $tid)
		{
			$to = $tid->getTo();
			$mall = (!empty($to)) ? "<div class='col-xs-6 contactText'>%s:</div><div class='col-xs-6 contactText'>%s - %s</div>" : "<div class='col-xs-6 contactText'>%s:</div><div class='col-xs-6 contactText'>%s</div>";
			$contact .= sprintf($mall,$tid->getTitle(),$tid->getFrom(),$tid->getTo());
			$contact .= "\n";
		}
		$contact .= "</div>";
		$contact .= "</div>\n";
        $contact .= "</div>\n";
	}
	

	$contact .= "<div class='clear'></div>\n";
}


	// Autobots assemble!
	

	$active = '';
?>


<div class="wrap container mainText subpage" role="document">
	<div class="row relative">
		<div class="col-xs-12 subHeader">
			<?php get_template_part('templates/page', 'header'); ?>
		</div>

		<script type="text/javascript">
			var responsiveMaps = [];
            var responsiveMap;
			var beachMarker;
			var mapOptions;
			var facility;
            var bounds = new google.maps.LatLngBounds();

            var mapLatLongs = [];
            <?php foreach ($latlongs as $facilityLatLong) { ?>
                mapLatLongs.push({
                    'lat': '<?= $facilityLatLong['lat']; ?>',
                    'long': '<?= $facilityLatLong['long']; ?>'
                });
            <?php } ?>

            <?php if(isset($_GET['a'])){ ?>
            facility = <?php echo $_GET['a']; ?>;
            <?php } ?>

		</script>
    </div>

    <div class="row relative minPageHeight">
        <div class='col-xs-12 col-md-8 subContent'>
            <?php foreach ($facilityArr as $key => $facility) { ?>
                <div class="facility-map">
                    <h2>Hitta till <?= $facility->getAddress()->getCity() ?></h2>
                    <div id="map-canvas-<?= $key; ?>" style="height: 310px"></div>
                </div>
            <?php } ?>
            
            <?= $staffHtml ?>
        </div>

        <!-- Contact details & form -->
        <div class='col-xs-12 col-md-4 subBorderLeft'>
            <?= $contact ?>
        </div>
    </div>

</div>