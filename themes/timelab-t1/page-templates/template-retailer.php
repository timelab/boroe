<?php
/*
Template Name: Återförsäljare Mall
*/

$args = array(
  'role' => 'reseller_installer'
);

// The Query
$user_query = new WP_User_Query( $args );
$arrComp = array();
$ret_html;

if ( ! empty( $user_query->results ) ) {
  foreach ( $user_query->results as $user ) { 
    $name = (esc_attr(get_the_author_meta( 't1-companyname', $user->ID ) ) == null) ? $user->display_name : esc_attr(get_the_author_meta( 't1-companyname', $user->ID ) );

    $comp = $name;
    $lat = esc_attr(get_the_author_meta( 't1-address_lat', $user->ID ) );
    $long = esc_attr(get_the_author_meta( 't1-address_long', $user->ID ) );
    $address = esc_attr(get_the_author_meta( 't1-address_street', $user->ID ) );
    $zip = esc_attr(get_the_author_meta( 't1-address_zip', $user->ID ) );
    $city = esc_attr(get_the_author_meta( 't1-address_city', $user->ID ) );
    $phone = esc_attr(get_the_author_meta( 't1-companyphone', $user->ID ) );
    $epost = esc_attr(get_the_author_meta( 't1-companyemail', $user->ID ) );
    $heating = esc_attr(get_the_author_meta( 't1-heatingproducts', $user->ID ) );
    $trinette = esc_attr(get_the_author_meta( 't1-trinette', $user->ID ) );
    $webshop = esc_attr(get_the_author_meta( 't1-webshop', $user->ID ) );
    array_push($arrComp, array($comp, $lat, $long, $address, $zip, $city, $phone, array($heating, $trinette, $webshop)) );

    //Build retailers html that is below the map

    $ret_html .= emitRetailers($user);

  }
}

?>

<div class="wrap container mainText PodsBrands subpage retailer-template" role="document">
  <div class="row relative">
      <div class="col-xs-12 subHeader">
        <?php get_template_part('templates/page', 'header'); ?>
      </div>
    </div>

    <script type="text/javascript">
        var locations = <?= json_encode($arrComp) ?>;
      </script>
    
    <div class="row relative minPageHeight">
      <div class="col-xs-12 subContent no-padding-height">
        <div class="map-container">
          <div class="search-retailer">
            <div class="prod-cat"><input type="checkbox" id="heatingproducts" name="heatingproducts"><label for="heatingproducts"><?php _e('Heating products','t1'); ?></label></div>
            <div class="prod-cat"><input type="checkbox" id="trinette" name="trinette"><label for="trinette"><?php _e('Trinette','t1'); ?></label></div>
            <div class="prod-cat"><input type="checkbox" id="webshop" name="webshop"><label for="webshop"><?php _e('Webshop','t1'); ?></label></div>
            <div class="right">
              <input type="text" name="zip" placeholder="Sök via postnummber" class="post-nmb">
            </div>
          </div>
          <div id="map" style="width: 100%; height: 480px;"></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-8 subContent .row-eq-height ret-container">
      <?php get_template_part('templates/content', 'page'); ?>
        <div class="retailers" data-ajax-url="<?= get_template_directory_uri(); ?>/templates/ajax-retailer.php">
          <?= $ret_html ?>
        </div>
      </div>
          
        <div class="col-md-4 subBorderLeft">
            <?php if ($haveParent || $isParent > 0){ ?>
                <div class="boxrelative">
                    <div class="contentboxtop"></div>
                    <div class="contentbox submenucontainer">
                        <?php
                        global $topParent;
                        wp_nav_menu( array('menu' => 'primary_navigation', 'menu_class' => 'submenu', 'depth' => 4, 'walker' => new JC_Walker_Nav_Menu($topParent)) );
                        ?>
                    </div>
                </div>
            <?php }else {
                echo emitShowcases_vertical($cms); //exists in t1-lib.php
            }

            echo emitAAA();

            ?>

      </div>
  </div>
</div>
