<div class="wrap container mainText PodsBrands subpage" role="document">
	<div class="row relative">
		<div class="col-xs-12 subHeader">
			<?php get_template_part('templates/page', 'header'); ?>
		</div>
	</div>

	<div class="row relative minPageHeight">
		<div class="col-sm-8 col-md-8 subContent subBorderRight">
			<?php
			//do_action( 'woocommerce_before_main_content' );
			woocommerce_content();
			//do_action( 'woocommerce_after_main_content' );
			?>
		</div>

		<div class="col-sm-4 col-md-4 shop-sidebar">
			<div class="view-btn"><i class="fa fa-spinner fa-spin"></i></div>
			<div class="row showcases collapsed">
				<div class="col-sm-12 shop-cat-menu">
					<ul class="widget-ul">
						<?php
							dynamic_sidebar('Shop Sidebar');
	            echo emitAAA();
						?>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>


